## Welcome to the dew - mean stack
The dew is minimalist software stack to provide a simple and fun starting point for cloud native fullstack javascript applications. The base of this project is based on mean.io and other advance features are introduced to help developer get most out of it.

### Installation 
``` 
git clone https://TheRealAmanChoudhary@bitbucket.org/TheRealAmanChoudhary/dew.git
cd dew
cp .env.example .env
npm start (for development)
```
### Docker based 
``` 
git clone https://TheRealAmanChoudhary@bitbucket.org/TheRealAmanChoudhary/dew.git
cd mean
cp .env.example .env
docker-compose up -d
```
