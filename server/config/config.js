const Joi = require('joi');

// require and configure dotenv, will load vars in .env in PROCESS.ENV
require('dotenv').config();

//Validate files through requests
const configSchema = rootRequire('requests/config/config');

const { error, value: envVars } = Joi.validate(process.env, configSchema);
if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

const config = {
  env: envVars.NODE_ENV,
  port: envVars.SERVER_PORT,
  mongooseDebug: envVars.MONGOOSE_DEBUG,
  debug: envVars.NODE_DEBUG,
  jwtSecret: envVars.JWT_SECRET,
  frontend: envVars.MEAN_FRONTEND || 'angular',
  apmService: envVars.APM_SERVICE,
  mongo: {
    host: envVars.MONGO_HOST,
    port: envVars.MONGO_PORT
  },
  owt: {
    service: envVars.OWT_ID,
    key: envVars.OWT_KEY,
    url: envVars.OWT_URL,
    hostname: envVars.OWT_HOST,
    port: envVars.OWT_PORT,
    protocol: envVars.OWT_PROTOCOL,
    httpPort: envVars.SERVER_PORT,
    httpsPort: envVars.SSL_PORT
  },
   captcha: {
        siteKey: envVars.SITE_KEY,
        secretKey: envVars.SECRET_KEY
  },

};

const meetingTypes = {
  1: 'open',
  2: 'semi-open',
  3: 'invite-only',
  4: 'private'
};

const audioTypes = {
  1: 'dial-in',
  2: 'computer-audio',
  3: 'both'
};

module.exports = config;
