const express = require('express');
const passport = require('passport');
const asyncHandler = require('express-async-handler');
const validateMiddleware = rootRequire('middlewares/validate.middleware');
const userController = rootRequire('controllers/user.controller');
const adminSchema = rootRequire('requests/admin/admin');

const router = express.Router();
module.exports = router;

router.use(passport.authenticate('jwt', {session: false}));

router.route('/users').post(validateMiddleware(adminSchema.user.create, 'body'), asyncHandler(userController.store));
router.route('/users').get(validateMiddleware(adminSchema.list, 'query'), asyncHandler(userController.index));
router.route('/users/:id').get(validateMiddleware(adminSchema.id, 'params'), asyncHandler(userController.show));
router.route('/users/:id').delete(validateMiddleware(adminSchema.id, 'params'), asyncHandler(userController.destroy));
router.route('/users/:id').put(validateMiddleware(adminSchema.id, 'query'), asyncHandler(userController.update));
