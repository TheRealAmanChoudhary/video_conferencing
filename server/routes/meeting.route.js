const express = require('express');
const passport = require('passport');
const asyncHandler = require('express-async-handler');
const validateMiddleware = rootRequire('middlewares/validate.middleware');
const meetingController = rootRequire('controllers/meeting.controller');
const meetingSchema = rootRequire('requests/meeting/meeting');
const meetingAttendeesController = rootRequire('controllers/meetingAttendees.controller');
const meetingAttendeesSchema = rootRequire('requests/meetingAttendees/meetingAttendees');

const router = express.Router();
module.exports = router;

//router.use(passport.authenticate('jwt', {session: false}));

router.route('/validate').post(validateMiddleware(meetingSchema.meeting.create, 'body'), asyncHandler(meetingController.validateMeeting));

router.route('/').post(validateMiddleware(meetingSchema.meeting.create, 'body'), asyncHandler(meetingController.store));
router.route('/').get(validateMiddleware(meetingSchema.list, 'query'), asyncHandler(meetingController.index));
router.route('/:id').get(validateMiddleware(meetingSchema.id, 'params'), asyncHandler(meetingController.show));
router.route('/:id').delete(validateMiddleware(meetingSchema.id, 'params'), asyncHandler(meetingController.destroy));
router.route('/:id').put(validateMiddleware(meetingSchema.id, 'params'), validateMiddleware(meetingSchema.meeting.update, 'body'), asyncHandler(meetingController.update));
router.route('/:meeting_id/meeting-attendees').post(validateMiddleware(meetingAttendeesSchema.meeting_id, 'params'), validateMiddleware(meetingAttendeesSchema.meetingAttendees.create, 'body'), asyncHandler(meetingAttendeesController.store));
router.route('/:meeting_id/meeting-attendees').get(validateMiddleware(meetingAttendeesSchema.meeting_id, 'params'), asyncHandler(meetingAttendeesController.index));
router.route('/:meeting_id/meeting-attendees/:id').get(validateMiddleware(meetingAttendeesSchema.id, 'params'), asyncHandler(meetingAttendeesController.show));
router.route('/:meeting_id/meeting-attendees/:id').delete(validateMiddleware(meetingAttendeesSchema.id, 'params'), asyncHandler(meetingAttendeesController.destroy));
router.route('/:meeting_id/meeting-attendees/:id').put(validateMiddleware(meetingAttendeesSchema.id, 'params'), validateMiddleware(meetingAttendeesSchema.meetingAttendees.update, 'body'), asyncHandler(meetingAttendeesController.update));
router.route('/:meeting_id/meeting-attendees/:id/feedback').post(validateMiddleware(meetingAttendeesSchema.id, 'params'), asyncHandler(meetingAttendeesController.addFeedback));

router.route('/exit-meeting').post(asyncHandler(meetingController.exitMeeting));
router.route('/:id/start').put(asyncHandler(meetingController.startMeeting));
router.route('/verify').post(asyncHandler(meetingController.verifyCaptcha));
