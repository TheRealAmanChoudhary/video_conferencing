const express = require('express');
const passport = require('passport');
const asyncHandler = require('express-async-handler')
const validateMiddleware = rootRequire('middlewares/validate.middleware');
const authController = rootRequire('controllers/auth.controller');
const authSchema = rootRequire('requests/auth/auth');

const router = express.Router();
module.exports = router;

router.post('/register', validateMiddleware(authSchema.register, 'body'), asyncHandler(authController.register), authController.login);
router.post('/login', validateMiddleware(authSchema.login, 'body'), passport.authenticate('local', {session: false}), authController.login);
router.get('/me', passport.authenticate('jwt', {session: false}), authController.login);
