const express = require('express');
const asyncHandler = require('express-async-handler');
//const validateMiddleware = rootRequire('middlewares/validate.middleware');
const owtController = rootRequire('controllers/owt.controller');
//const owtSchema = rootRequire('requests/auth/auth');
const Owt = rootRequire('models/owt.model');

const router = express.Router();
module.exports = router;

router.get('/', asyncHandler(owtController.index));
router.post('/', asyncHandler(owtController.store));
router.post('/:room_id/tokens', asyncHandler(owtController.createToken));
router.delete('/:room_id', asyncHandler(owtController.deleteRoom));
router.post('/:room_id/join', asyncHandler(owtController.joinRoom));
router.delete('/:room_id/participants/:participant_id', asyncHandler(owtController.deleteParticipant));
router.delete('/:room_id/streams/:stream_id', asyncHandler(owtController.deleteStream));

// Route internal REST interface
router.use(async function (req, res) {
  console_log(req.path);
  console.log(req.body, req.params);
  await Owt.request(req.method, '/v1/rooms' + req.path, req.body)
    .then((imRes) => {
      res.writeHead(imRes.statusCode, imRes.headers);
      imRes.pipe(res);
    })
    .catch((err) => {
      console_log(err);
    });
});
