const express = require('express');

const authRoutes = require('./auth.route');
const clientRoutes = require('./client.route');
const adminRoutes = require('./admin.route');
const owtRoutes = require('./owt.route');
const meetingRoutes = require('./meeting.route');

const router = express.Router(); // eslint-disable-line new-cap

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK')
);

router.use('/meetings', meetingRoutes);

router.use('/auth', authRoutes);
router.use('/owt', owtRoutes);
router.use('/', clientRoutes);

//Authenticated routes
router.use('/admin', adminRoutes);

module.exports = router;
