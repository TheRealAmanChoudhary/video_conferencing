const config = rootRequire('config/config');
const Log = rootRequire('models/log.model');
const User = rootRequire('models/user.model');

const log_level = {
  0: 'LOG',
  1: 'INFO',
  2: 'WARNING',
  3: 'ERROR',
  4: 'DIE'
};

//TODO: Beautify console logs
let console_log = function () {
  if (config.debug) {
    let message = '';

    if (typeof arguments[1] == 'undefined') {
      arguments[1] = 0;
    }

    if (typeof arguments[1] == 'number') {
      message = '[' + log_level[arguments[1]] + ']: ';
      if (typeof arguments[0] == 'string') {
        message += arguments[0];
      } else {
        message = arguments;
      }
    } else {
      message = arguments;
    }

    console.log(message);
  }
};

let console_die = function(){
  console_log(arguments[0],4);
  process.exit();
};

let logActivity = async function(req, data) {
  if (req.originalMethod != null && req.originalMethod.toUpperCase() !== 'GET') {
    let method = getDescription(req.originalMethod);
    let loggedInUser = req.user;
    let dateTime = getDateTime(new Date());
    let log = {
      log_name: data.log_name == null ? null : data.log_name,
      description: method,
      causer_id: loggedInUser == null ? null : loggedInUser._id,
      causer_type: loggedInUser == null ? null : User.collection.collectionName,
      properties: {request: req.body, response: data.response == null ? null : data.response},
      message: (data.log_name == null ? '' : data.log_name) + ' ' + method + (loggedInUser == null ? '' : ' by ' + loggedInUser.fullname) + ' on ' + dateTime.date + ' at ' + dateTime.time,
      url: req.originalUrl == null ? null : req.originalUrl,
      ip_address: req._remoteAddress == null ? null : req._remoteAddress,
      user_agent: req.headers['user-agent'] == null ? null : req.headers['user-agent']
    };
    if (data.response._id) {
      log.subject_id = data.response._id;
    }
    if (data.type) {
      log.subject_type = data.type.collection.collectionName;
    }
    await new Log(log).save();
  }
};

let getDateTime = function(date) {
  let hour = date.getHours();
  hour = (hour < 10 ? '0' : '') + hour;
  let min  = date.getMinutes();
  min = (min < 10 ? '0' : '') + min;
  let sec  = date.getSeconds();
  sec = (sec < 10 ? '0' : '') + sec;
  let year = date.getFullYear();
  let month = date.getMonth() + 1;
  month = (month < 10 ? '0' : '') + month;
  let day  = date.getDate();
  day = (day < 10 ? '0' : '') + day;
  return {
    date: day + '-' + month + '-' + year,
    time: hour + ':' + min + ':' + sec,
    date_time: date + '-' + month + '-' + year + ' ' + hour + ':' + min + ':' + sec
  };
};

function getDescription(method) {
  method = method.toUpperCase();
  if (method === 'POST') return 'created';
  else if (method === 'PUT' || method === 'PATCH') return 'updated';
  else if (method === 'DELETE') return 'deleted';
  else return method;
}

global.config = config;
global.console_log = console_log;
global.console_die = console_die;
global.logActivity = logActivity;
global.getDateTime = getDateTime;
