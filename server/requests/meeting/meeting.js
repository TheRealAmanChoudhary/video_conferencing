const Joi = require('joi');

let schemas = {
  list: {
    name: Joi.string(),
    meeting_code: Joi.string(),
    page: Joi.number(),
    page_size: Joi.number()
  },
  id: {
    id: Joi.string().required().regex(/^[0-9a-fA-F]{24}$/, 'required').error(() => {
      return {
        message: 'invalid id.',
      }
    }),
  },
  meeting: require('./meeting.schema'),
};

module.exports = schemas;
