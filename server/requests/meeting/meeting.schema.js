const Joi = require('joi');

const schemas = {
  create: Joi.object({
    name: Joi.string().required(),
    display_name: Joi.string(),
    host_name: Joi.string(),
    description: Joi.string(),
    type: Joi.string(),//.required(),
    meeting_code: Joi.string(),
    status: Joi.bool(),
    max_concurrent_participants: Joi.number(),
    owt_meeting_id: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
    audio_type: Joi.string(),
    host_video: Joi.bool(),
    participant_video: Joi.bool(),
    enable_waiting_room: Joi.bool(),
    enable_join_before_host: Joi.bool(),
    mute_participants_upon_entry: Joi.bool(),
    agree_tnc: Joi.bool(),
    client_id: Joi.string(),
    meeting_started: Joi.date(),
    end_meeting_for_all: Joi.bool()
  }),
  update: Joi.object({
    name: Joi.string(),
    display_name: Joi.string(),
    host_name: Joi.string(),
    description: Joi.string(),
    type: Joi.string(),
    meeting_code: Joi.string(),
    status: Joi.bool(),
    duration: Joi.string().regex(/^([0-9]{2}):[0-5][0-9]:[0-5][0-9]$/),
    max_concurrent_participants: Joi.number(),
    audio_type: Joi.string(),
    host_video: Joi.bool(),
    participant_video: Joi.bool(),
    enable_waiting_room: Joi.bool(),
    enable_join_before_host: Joi.bool(),
    mute_participants_upon_entry: Joi.bool(),
    agree_tnc: Joi.bool(),
    meeting_started: Joi.date(),
    meeting_ended: Joi.date(),
    host_left_meeting_at: Joi.date(),
    end_meeting_for_all: Joi.bool()
  })
};


module.exports = schemas;
