const Joi = require('joi');

const schemas = {
  create: Joi.object({
    name: Joi.string().required(),
    user_id: Joi.number(),
    added_by: Joi.number(),
    client_id: Joi.string(),
    rating: Joi.number(),
    feedback: Joi.string(),
    role: Joi.string(),
    contact: Joi.number(),
    attendee_details: Joi.object(),
    ip_address: Joi.string(),
    bandwidth: Joi.string(),
    created_at: Joi.date(),
    updated_at: Joi.date()
  }),
  update: Joi.object({
    name: Joi.string(),
    rating: Joi.number(),
    feedback: Joi.string().allow(""),
    role: Joi.string(),
    contact: Joi.number(),
    attendee_details: Joi.object(),
    ip_address: Joi.string(),
    bandwidth: Joi.string(),
    created_at: Joi.date(),
    updated_at: Joi.date(),
    deleted_at: Joi.date()
  })
};

module.exports = schemas;
