const Joi = require('joi');

let schemas = {
  list: {
    page: Joi.number().required(),
    page_size: Joi.number().required()
  },
  meeting_id: {
    meeting_id: Joi.string().required().regex(/^[0-9a-fA-F]{24}$/, 'required').error(() => {
      return {
        message: 'invalid meeting id.',
      }
    }),
  },
  id: {
    id: Joi.string().required().regex(/^[0-9a-fA-F]{24}$/, 'required').error(() => {
      return {
        message: 'invalid id.',
      }
    }),
    meeting_id: Joi.string().required().regex(/^[0-9a-fA-F]{24}$/, 'required').error(() => {
      return {
        message: 'invalid meeting id.',
      }
    }),
  },
  meetingAttendees: require('./meetingAttendees.schema')
};

module.exports = schemas;
