const Joi = require('joi');

const schemas = {
  create: Joi.object({
    full_name: Joi.string().required(),
    email: Joi.string().email(),
    mobile_number: Joi.string().regex(/^[1-9][0-9]{9}$/),
    password: Joi.string().required(),
    repeat_password: Joi.string().required().valid(Joi.ref('password')),
    client_id: Joi.string().required(),
    type: Joi.number().required(),
    role: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
    permission: Joi.object(),
    email_verified_at: Joi.date(),
    password_changed_at: Joi.date(),
    confirmation_code: Joi.string(),
    confirmed: Joi.number(),
    last_login_at: Joi.date(),
    last_login_ip: Joi.string()
  })
};

module.exports = schemas;
