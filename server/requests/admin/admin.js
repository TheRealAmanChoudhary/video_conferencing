const Joi = require('joi');

let schemas = {
  list: {
    page: Joi.number().required(),
    pageSize: Joi.number().required()
  },
  id: {
    id: Joi.string().required().regex(/^[0-9a-fA-F]{24}$/, 'required').error(() => {
      return {
        message: 'invalid id.',
      }
    }),
  },
  user: require('./user.schema')
};

module.exports = schemas;
