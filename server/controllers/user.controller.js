const bcrypt = require('bcrypt');
const User = rootRequire('models/user.model');

module.exports = {
  index,
  store,
  show,
  update,
  destroy
};

function index(req, res) {
  console_log('User index is called.');
  res.json('User index is called.');
}

async function store(req, res) {
  let user = req.body;
  user.hashed_password = bcrypt.hashSync(user.password, 10);
  delete user.password;
  delete user.repeatPassword;
  user = await new User(user).save();
  let data = {
    log_name: 'User',
    type: User,
    response: user
  };
  await logActivity(req, data);
  return res.json(user);
}

function show(req, res) {
  console_log('User show is called.');
  res.json('User show is called.');
}

function update(req, res) {
  console_log('User update is called.');
  res.json('User update is called.');
}

function destroy(req, res) {
  console_log('User destroy is called.');
  res.json('User destroy is called.');
}
