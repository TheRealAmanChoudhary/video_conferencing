const Owt = rootRequire('models/owt.model');

module.exports = {
  index,
  store,
  show,
  update,
  destroy,
  createToken,
  deleteRoom,
  deleteParticipant,
  deleteStream
};

async function index(req, res) {
  let meetingRoom = req.body;

  const checkResponse = (resp) => {
    const rooms = JSON.parse(resp);
    let errorString = "Rooms not found";

    if (typeof rooms === "object" && Object.keys(rooms).length > 0) {

      if (Object.keys(meetingRoom).length > 0 && typeof meetingRoom.name !== undefined) {
        let searchedRoom = searchRoom(meetingRoom.name, rooms);
        if (searchedRoom) {
          res.json(searchedRoom);
        } else {
          res.json("Room not found");
        }
      } else {
        res.json(rooms);
      }
    } else {
      res.json(errorString);
    }
  };
  await Owt.request('GET', '/v1/rooms?page=1&per_page=100', null, checkResponse, onRequestFail);
}

function searchRoom(name, rooms) {
  for (const room of rooms) {
    if (room.name === name) {
      return room;
    }
  }
  return false;
}

async function store(req, res) {
  let meetingRoom = req.body;

  const createBody = JSON.stringify({
    name: meetingRoom.name,
    options: {
      "mediaIn": {
        "video": [
          {
            "codec": "h264"
          },
          {
            "codec": "vp8"
          },
          {
            "codec": "vp9"
          }
        ],
        "audio": [
          {
            "channelNum": 2,
            "sampleRate": 48000,
            "codec": "opus"
          }
        ]
      },
      "mediaOut": {
        "video": {
          "parameters": {
            "keyFrameInterval": [
              100,
              30,
              5,
              2,
              1
            ],
            "bitrate": [
              "x0.8",
              "x0.6",
              "x0.4",
              "x0.2"
            ],
            "framerate": [
              6,
              12,
              15,
              24,
              30,
              48,
              60
            ],
            "resolution": [
              "x3/4",
              "x2/3",
              "x1/2",
              "x1/3",
              "x1/4",
              "hd1080p",
              "hd720p",
              "svga",
              "vga",
              "qvga",
              "cif"
            ]
          },
          "format": [
            {
              "codec": "vp8"
            },
            {
              "profile": "CB",
              "codec": "h264"
            },
            {
              "profile": "B",
              "codec": "h264"
            },
            {
              "codec": "vp9"
            }
          ]
        },
        "audio": [
          {
            "channelNum": 2,
            "sampleRate": 48000,
            "codec": "opus"
          }
        ]
      }
    }
  });

  const checkResponse = (resp) => {
    const room = JSON.parse(resp);
    res.json(room);
  };

  await Owt.request('POST', '/v1/rooms', createBody, checkResponse, onRequestFail);
}

async function createToken(req, res) {
  const tokenRoom = req.params.room_id;
  await Owt.request('POST', '/v1/rooms/' + tokenRoom + '/tokens', req.body, (token) => {
    res.json({token: token});
    //console.log(res);
  }, (err) => {
    res.send(err);
  })
}

async function deleteRoom(req, res) {
  const roomID = req.params.room_id;
  await Owt.request('DELETE', '/v1/rooms/' + roomID, req.body, (resp) => {
    res.json({success: true});
  }, (err) => {
    res.send(err);
  })
}

async function deleteParticipant(req, res) {
  const roomID = req.params.room_id;
  const participantID = req.params.participant_id;
  await Owt.request('DELETE', '/v1/rooms/' + roomID + '/participants/' + participantID, req.body, (resp) => {
    let socketio = req.app.get('socketio'); // take out socket instance from the app container
    socketio.sockets.emit('participant', {
      id: participantID
    }); // emit an event for all connected clients
    res.json({success: true});
  }, (err) => {
    res.send(err);
  })
}

async function deleteStream(req, res) {
  const roomID = req.params.room_id;
  const streamID = req.params.stream_id;
  await Owt.request('DELETE', '/v1/rooms/' + roomID + '/streams/' + streamID, req.body, (resp) => {
    let socketio = req.app.get('socketio'); // take out socket instance from the app container
    socketio.sockets.emit('stream', {
      id: streamID
    }); // emit an event for all connected clients
    res.json({success: true});
  }, (err) => {
    res.send(err);
  })
}

function onRequestFail(err) {
  console_log(err, 3);
}

function show(req, res) {
  console_log('Owt show is called.');
  res.json('Owt show is called.');
}

function update(req, res) {
  console_log('Owt update is called.');
  res.json('Owt update is called.');
}

function destroy(req, res) {
  console_log('Owt destroy is called.');
  res.json('Owt destroy is called.');
}

//findRoom returns Promise
/*async function findRoom(meetingRoom) {
  return new Promise((onOk, onError) => {
    const checkResponse = (resp) => {
      const rooms = JSON.parse(resp);
      let sampleRoomId = null;
      let sampleRoom = {};

      //console_log(rooms);
      // Find meeting room
      for (const room of rooms) {
        if (room.name === meetingRoom.name) {
          sampleRoomId = room._id;
          sampleRoom = room;
          break;
        }
      }

      if (sampleRoomId) {
        onOk(sampleRoom);
      } else {
        onError("Room not found!");
      }
    };

    await Owt.request('GET', '/v1/rooms?page=1&per_page=100', null, checkResponse, onError);
  });
}*/

/*async function findRoom(req, res) {
  let meetingRoom = req.body;

  const checkResponse = (resp) => {
    const rooms = JSON.parse(resp);
    let sampleRoomId = null;
    let sampleRoom = {};

    //console_log(rooms);
    // Find meeting room
    for (const room of rooms) {
      if (room.name === meetingRoom.name) {
        sampleRoomId = room._id;
        sampleRoom = room;
        break;
      }
    }

    if (sampleRoomId) {
      res.json(sampleRoom);
    } else {
      res.json("Room not found!");
    }
  };

  await Owt.request('GET', '/v1/rooms?page=1&per_page=100', null, checkResponse, onRequestFail);

}*/
