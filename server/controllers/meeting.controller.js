const Meeting = rootRequire('models/meeting.model');
const MeetingAttendees = rootRequire('models/meetingAttendees.model');
const Owt = rootRequire('models/owt.model');
const fetch = require("node-fetch");
// const secret = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe';
const secret = config.captcha.secretKey;

module.exports = {
  index,
  store,
  show,
  update,
  destroy,
  validateMeeting,
  exitMeeting,
  startMeeting,
  verifyCaptcha
};

async function index(req, res) {
  let options = {};
  if (req.query) {
    options = req.query;
  }
  options.deleted_at = null;
  console.log(options);
  let allMeetings = await Meeting.find(options);
  return res.json(allMeetings);
}

async function store(req, res) {
  let meeting = req.body;
  console.log(meeting);
  try {
    meeting = await new Meeting(meeting).save();
    //req.meeting = meeting;
    return res.json(meeting);
  } catch (err) {
    console_log(err);
    return res.json({error: 'something went wrong'});
  }
}

async function show(req, res) {
  meeting = await Meeting.findOne({_id: req.params.id, deleted_at: null});
  return res.json(meeting);
}

async function update(req, res) {
  let meeting = req.body;
  meeting.updated_at = Date.now();
  try {
    let update = await Meeting.findOneAndUpdate({_id: req.params.id}, {
      $set: meeting
    }, {new: true});
    if (update) {
      return res.json(update);
    }
    return res.json({error: "Something wrong when updating data!"});
  } catch (err) {
    console_log(err);
    return res.json({error: 'something went wrong'});
  }
}

async function destroy(req, res) {
  try {
    let update = await Meeting.findOneAndUpdate({_id: req.params.id}, {
      $set: {
        deleted_at: Date.now()
      }
    }, {new: true});
    if (update) {
      return res.json(update);
    }
    return res.json({error: "Something wrong when deleting data!"});
  } catch (err) {
    console_log(err);
    return res.json({error: 'something went wrong'});
  }
}

async function validateMeeting(req, res) {
  let meeting = await Meeting.findOne({name: req.body.name, meeting_code: req.body.meeting_code, deleted_at: null});
  return res.json({data: meeting});
}

async function exitMeeting(req, res) {
  const attendeeID = req.body.attendee_id;
  const owtAttendeeID = req.body.owt_attendee_id;
  const meetingID = req.body.meeting_id;
  const owtMeetingID = req.body.owt_meeting_id;
  const attendeeCount = req.body.attendee_count;
  try {
    const removedOwtAttendee = await Owt.request('DELETE', '/v1/rooms/' + owtMeetingID + '/participants/' + owtAttendeeID, {}, (res) => {
      // console.log(res);
    }, (err) => {
      // console.log('err');
    });

    const removedAttendee = await MeetingAttendees.findOneAndUpdate({_id: attendeeID}, {
      $set: {
        deleted_at: Date.now()
      }
    }, {new: true});
    if (attendeeCount === 1) {
      const deleteOwtRoom = await Owt.request('DELETE', '/v1/rooms/' + owtMeetingID, {}, (res) => {
        // console.log(res);
      }, (err) => {
        // console.log('err');
        // res.send(err);
      });
      const deletedRoom = await Meeting.findOneAndUpdate({_id: meetingID}, {
        $set: {
          deleted_at: Date.now()
        }
      }, {new: true});
    }


  } catch (e) {
    console.log(e)
  }

  console.log(req.body);
}

async function startMeeting(req, res) {
  try {
    let update = await Meeting.findOneAndUpdate({_id: req.params.id}, {
      $set: {
        started_at: Date.now()
      }
    }, {new: true});
    if (update) {
      return res.json(update);
    }
    return res.json({error: "Something wrong when deleting data!"});
  } catch (err) {
    console_log(err);
    return res.json({error: 'something went wrong'});
  }
}

async function verifyCaptcha(req, res) {
  const secret_key = secret;
  const token = req.body.token;
  const url = `https://www.google.com/recaptcha/api/siteverify?secret=${secret_key}&response=${token}`;

  fetch(url, {
    method: 'post'
  })
    .then(response => response.json())
    .then(google_response => res.json({ google_response }))
    .catch(error => res.json({ error }));
}
