const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = rootRequire('config/config');
const User = rootRequire('models/user.model');

module.exports = {
  register,
  login
};

async function register(req, res, next) {
  let user = await store(req.body);
  user = user.toObject();
  delete user.hashedPassword;
  let data = {
    log_name: 'User',
    type: User,
    response: user
  };
  await logActivity(req, data);
  req.user = user;
  next()
}

function login(req, res) {
  let user = req.user;
  let token = generateToken(user);

  res.json({user, token});
}

function generateToken(user) {
  const payload = JSON.stringify(user);
  return jwt.sign(payload, config.jwtSecret);
}

async function store(user) {
  user.hashedPassword = bcrypt.hashSync(user.password, 10);
  delete user.password;
  delete user.repeatPassword;
  return await new User(user).save();
}
