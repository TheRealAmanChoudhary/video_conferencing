const MeetingAttendees = rootRequire('models/meetingAttendees.model');
const Feedback = rootRequire('models/feedback.model');

module.exports = {
  index,
  store,
  show,
  update,
  destroy,
  addFeedback
};

async function index(req, res) {
  let allMeetingAttendees;
  allMeetingAttendees = await MeetingAttendees.find({meeting_id: req.params.meeting_id, "deleted_at":null});
  return res.json(allMeetingAttendees);
}

async function store(req, res) {
  let meetingAttendees = req.body;
  try{
    meetingAttendees.meeting_id = req.params.meeting_id;
    meetingAttendees = await new MeetingAttendees(meetingAttendees).save();
    return res.json(meetingAttendees);
  } catch (err) {
    console_log(err);
    return res.json({error: 'something went wrong'});
  }
}

async function show(req, res) {
  meetingAttendees = await MeetingAttendees.findOne({_id: req.params.id, meeting_id: req.params.meeting_id, deleted_at:null});
  return res.json(meetingAttendees);
}

async function update(req, res) {
  let meetingAttendees = req.body;
  meetingAttendees.updated_at = Date.now();
  try {
    let update = await MeetingAttendees.findOneAndUpdate({_id: req.params.id}, {
      $set: meetingAttendees
    },{new: true});
    if (update) {
      /*let socketio = req.app.get('socketio'); // take out socket instance from the app container

      socketio.sockets.emit('attendeeupdated', {
        id: req.params.id,
        meetingAttendees
      }); // emit an event for all connected clients*/
      return res.json(update);
    }
    return res.json({error: "Something wrong when updating data!"});
  } catch (err) {
    console_log(err);
    return res.json({error: 'something went wrong'});
  }
}

async function destroy(req, res) {
  try{
    let update = await MeetingAttendees.findOneAndUpdate({_id: req.params.id}, {
      $set: {
        deleted_at: Date.now()
      }
    },{new: true});
    let socketio = req.app.get('socketio'); // take out socket instance from the app container
    socketio.sockets.emit('attendee', {
      id: req.params.id
    }); // emit an event for all connected clients
    if (update) {
      return res.json(update);
    }
    return res.json({error: "Something wrong when deleting data!"});
  } catch(err) {
    console_log(err);
    return res.json({error: 'something went wrong'});
  }
}

async function addFeedback(req, res) {
  let feedback = req.body;
  try{
    feedback.created_at = new Date().toISOString();
    feedback.meeting_id = req.params.meeting_id;
    if (feedback.anonymousCheck && feedback.anonymousCheck === false) {
      feedback.attendee_id = req.params.id;
    }
    feedback = await new Feedback(feedback).save();
    return res.json(feedback);
  } catch (err) {
    console_log(err);
    return res.json({error: 'something went wrong'});
  }
}

