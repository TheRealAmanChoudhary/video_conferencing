const mongoose = require('mongoose');

const FeedbackSchema = new mongoose.Schema({
  meeting_id: {
    type: String
  },
  attendee_id: {
    type: String
  },
  rating: {
    type: Number
  },
  feedback: {
    type: String
  },
  name: {
    type: String
  },
  email: {
    type: String
  },
  phoneNo: {
    type: String
  },
  created_at: {
      type: String,
      default: new Date().toISOString(),
  }

}, {
  versionKey: false
});


module.exports = mongoose.model('Feedback', FeedbackSchema);
