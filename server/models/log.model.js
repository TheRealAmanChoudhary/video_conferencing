const mongoose = require('mongoose');

const LogSchema = new mongoose.Schema({
  log_name: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  subject_id: {
    type: String,
  },
  subject_type: {
    type: String,
  },
  causer_id: {
    type: String,
  },
  causer_type: {
    type: String,
  },
  properties: {

  },
  message: {
    type: String,
  },
  url: {
    type: String,
  },
  ip_address: {
    type: String,
  },
  user_agent: {
    type: String,
  },
  created_at: {
    type: Date,
    default: Date.now
  }
}, {
  versionKey: false
});


module.exports = mongoose.model('Log', LogSchema);
