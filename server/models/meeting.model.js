const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MeetingSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  display_name: {
    type: String
  },
  description: {
    type: String,
  },
  host_name: {
    type: String,
  },
  type: {
    type: String,
    default: 'open'
  },
  meeting_code: {
    type: String,
  },
  started_at: {
    type: Date,
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  status: {
    type: Boolean,
  },
  duration: {
    type: String,
    match: [/^([0-9]{2}):[0-5][0-9]:[0-5][0-9]$/, 'please enter a valid duration']
  },
  max_concurrent_participants: {
    type: Number,
    default: -1,
  },
  owt_meeting_id: {
    type: String,
  },
  audio_type: {
    type: String
  },
  host_video: {
    type: Boolean,
    default: true
  },
  participant_video: {
    type: Boolean,
    default: true
  },
  enable_waiting_room: {
    type: Boolean,
    default: true
  },
  enable_join_before_host: {
    type: Boolean,
    default: false
  },
  mute_participants_upon_entry: {
    type: Boolean,
    default: false
  },
  agree_tnc: {
    type: Boolean,
    default: false
  },
  client_id: {
    type: String
  },
  meeting_started: {
    type: Date
  },
  meeting_ended: {
    type: Date
  },
  host_left_meeting_at: {
    type: Date
  },
  end_meeting_for_all: {
    type: Boolean,
    default: false
  },
  updated_at: {
    type: Date,
    default: Date.now()
  },
  deleted_at: {
    type: Date
  }

}, {
  versionKey: false
});


module.exports = mongoose.model('Meeting', MeetingSchema);
