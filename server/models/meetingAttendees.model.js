const mongoose = require('mongoose');

const MeetingAttendeesSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  user_id: {
    type: Number,
  },
  meeting_id: {
    type: String
  },
  added_by: {
    type: Number
  },
  client_id: {
    type: String,
  },
  rating: {
    type: Number
  },
  feedback: {
    type: String
  },
  role: {
    type: String,
    default: 'host'
  },
  contact: {
    type: Number,
  },
  attendee_details: {
    type: Object
  },
  ip_address: {
    type: String
  },
  bandwidth: {
    type: String,
  },
  created_at: {
    type: Date,
    default: Date.now(),
  },
  updated_at: {
    type: Date,
    default: Date.now()
  },
  deleted_at: {
    type: Date
  },

}, {
  versionKey: false
});


module.exports = mongoose.model('MeetingAttendees', MeetingAttendeesSchema);
