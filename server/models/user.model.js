const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  full_name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true,
    // Regexp to validate emails with more strict rules as added in tests/users.js which also conforms mostly with RFC2822 guide lines
    match: [/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 'Please enter a valid email'],
  },
  hashed_password: {
    type: String,
    required: true
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  roles: {
    type: String,
  },
  permission: {
    type: Object,
  },
  client_id: {
    type: String,
    required: true,
  },
  email_verified_at: {
    type: Date,
  },
  password_changed_at: {
    type: Date,
  },
  type: {
    type: Number,
    required: true,
  },
  confirmation_code: {
    type: String
  },
  confirmed: {
    type: Number
  },
  last_login_at: {
    type: Date
  },
  last_login_ip: {
    type: String
  }
}, {
  versionKey: false
});


module.exports = mongoose.model('User', UserSchema);
