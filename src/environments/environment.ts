// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  rootUrl: 'http://localhost:4040',
  owtUrl: 'http://localhost:4040',
  siteKey: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
  roomConfig: {
    iceServers: [{
      urls: 'stun:example.com:3478'
    }, {
      urls: [
        'turn:example.com:3478?transport=udp',
        'turn:example.com:3478?transport=tcp'
      ],
      credential: 'password',
      username: 'username'
    }]
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
