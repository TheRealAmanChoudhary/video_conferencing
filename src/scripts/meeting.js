const Owt = require('./owt.js');
const request = require('./request.js');

// License
var localStream = null;
var localScreen = null;
var localName = 'Anonymous';
var localId = null;
var localScreenId = null;
var users = [];
var progressTimeOut = null;
var smallRadius = 60;
var largeRadius = 120;
var isMouseDown = false;
var mouseX = null;
var mouseY = null;
var MODES = {
  GALAXY: 'galaxy',
  MONITOR: 'monitor',
  LECTURE: 'lecture'
};
var mode = MODES.GALAXY;
var SUBSCRIBETYPES = {
  FORWARD: 'forward',
  MIX: 'mix'
};
var subscribeType = SUBSCRIBETYPES.FORWARD;
var isScreenSharing = false;
var isLocalScreenSharing = false;
var remoteScreen = null;
var remoteScreenName = null;
var isMobile = false;
var streamObj = {};
var streamIndices = {};
var hasMixed = false;
var isSmall = false;
var isPauseAudio = true;
var isPauseVideo = false;
var isOriginal = true;
var isAudioOnly = false;

var showInfo = null;
var showLevel = null;
var scaleLevel = 3 / 4;
var refreshMute = null;

var currentRegions = null;

var localPublication = null;
var localScreenPubliction = null;
var joinResponse = null;

var localResolution = null;
var remoteMixedSub = null;
var subList = {};
var screenSub = null;

var room = null;
var roomId = null;

const remoteStreamMap = new Map();
const forwardStreamMap = new Map();

exports.joinRoom = function joinRoom(roomID) {
  request.createToken(roomID, 'Aman', 'presenter', (response) => {
    console.log(response);
    const token = response;
    if (!room) {
      room = new Owt.Conference.ConferenceClient();
      console.log(room);
      //addRoomEventListener();
    }
    /*console.log(new Owt.Base.Resolution(320, 240));
    const room = new Owt.Conference.ConferenceClient();
    room.join(token).then((res) => {
        console.log(res);
      }
    ).catch((err) => {
      console.log(err);
    });*/
  });
}
/*
function addRoomEventListener() {
  room.addEventListener('streamadded', (streamEvent) => {
    console.log('streamadded', streamEvent);
    var stream = streamEvent.stream;

    if (localStream && localStream.id === stream.id) {
      return;
    }
    if (stream.source.audio === 'mixed' && stream.source.video === 'mixed') {
      if (subscribeType !== SUBSCRIBETYPES.MIX) {
        return;
      }
      // subscribe mix stream
      thatName = "MIX Stream";
    } else {
      if (stream.source.video === 'screen-cast') {
        thatName = "Screen Sharing";
        if (isLocalScreenSharing) {
          return;
        }
      } else if (subscribeType !== SUBSCRIBETYPES.FORWARD) {
        return;
      }
    }

    var thatId = stream.id;
    if (stream.source.audio === 'mixed' && stream.source.video === 'mixed') {
      thatName = "MIX Stream";
    } else if ( stream.source.video === 'screen-cast') {
      thatName = "Screen Sharing";
    }

    // add video of non-local streams
    if (localId !== thatId && localScreenId !== thatId && localName !== getUserFromId(stream.origin).userId) {
      subscribeStream(stream);
    }
  });

  room.addEventListener('participantjoined', (event) => {
    console.log('participantjoined', event);
    if(event.participant.userId !== 'user' && getUserFromId(event.participant.id) === null){
      //new user
      users.push({
        id: event.participant.id,
        userId: event.participant.userId,
        role: event.participant.role
      });
      event.participant.addEventListener('left', () => {
        if(event.participant.id !== null && event.participant.userId !== undefined){
          sendIm(event.participant.userId + ' has left the room ', 'System');
          deleteUser(event.participant.id);
        } else {
          sendIm('Anonymous has left the room.', 'System');
        }
      });
      console.log("join user: " + event.participant.userId);
      addUserListItem(event.participant,true);
      //no need: send message to all for initId
    }
  });

  room.addEventListener('messagereceived', (event) => {
    console.log('messagereceived', event);
    var user = getUserFromId(event.origin);
    if (!user) return;
    var receivedMsg = JSON.parse(event.message);
    if(receivedMsg.type == 'msg'){
      if(receivedMsg.data != undefined) {
        var time = new Date();
        var hour = time.getHours();
        hour = hour > 9 ? hour.toString() : '0' + hour.toString();
        var mini = time.getMinutes();
        mini = mini > 9 ? mini.toString() : '0' + mini.toString();
        var sec = time.getSeconds();
        sec = sec > 9 ? sec.toString() : '0' + sec.toString();
        var timeStr = hour + ':' + mini + ':' + sec;
        var color = getColor(user.userId);
        $('<p class="' + color + '">').html(timeStr + ' ' + user.userId +'<br />')
          .append(document.createTextNode(receivedMsg.data)).appendTo('#text-content');
        $('#text-content').scrollTop($('#text-content').prop('scrollHeight'));
      }
    }
  });
}

function subscribeStream(stream) {
  console.info('subscribing:', stream.id);
  var videoOption = !isAudioOnly;
  room.subscribe(stream, {video: videoOption}).then(subscription => {
    console.info('subscribed: ',subscription.id);
    addVideo(stream, false);
    subList[subscription.id] = subscription;
    console.info("add success");
    streamObj[stream.id] = stream;
    if(stream.source.video === 'mixed'){
      remoteMixedSub = subscription;
    }
    if(stream.source.video === 'screen-cast'){
      screenSub = subscription;
      stream.addEventListener('ended', function(event) {
        changeMode(MODES.LECTURE);
        setTimeout(function() {
          $('#local-screen').remove();
          $('#screen').remove();
          shareScreenChanged(false, false);
          if (subscribeType === SUBSCRIBETYPES.MIX) {
            changeMode(mode, $("div[isMix=true]"));
          } else {
            changeMode(mode);
          }
        }, 800);
      });
    }
    setTimeout(function() {
      subscription.getStats().then( report => {
        console.info(report);
        report.forEach(function(item,index){
          if(item.type === 'ssrc' && item.mediaType === 'video'){
            scaleLevel = parseInt(item.googFrameHeightReceived)/parseInt(item.googFrameWidthReceived);
            console.info(scaleLevel);
          }
        });
        resizeStream(mode);
      }, err => {
        console.error('stats error: ' + err);
      });
    }, 1000);
    // monitor(subscription);
  }, err => {
    console.error('subscribe error: ' + err);
  });
}

function addVideo(stream, isLocal) {
  // compute next html id
  //var id = $('#video-panel').children('.client').length;
  var id = stream.id;
  console.log("addVideo video panel client length:", id);
  //while ($('#client-' + id).length > 0) {
  //++id;
  //}
  var uid = stream.origin;
  if (isLocal) {
    console.log("localStream addVideo1");
  }

  // check if is screen sharing
  if (stream.source.video === 'screen-cast') {
    $('#video-panel').addClass('screen')
      .append('<div class="client" id="screen"></div>');
    $('#screen').append('<video id="remoteScreen" playsinline autoplay class="palyer" style="width:100%;height:100%"></video>');
    $('#remoteScreen').get(0).srcObject = stream.mediaStream;
    // stream.show('screen');
    $('#screen').addClass('clt-' + getColorId(uid))
      .children().children('div').remove();
    $('#video-panel .largest').removeClass("largest");
    $('#screen').addClass("largest");
    $('#screen').append(
      '<div class="ctrl" id="original"><a href="#" class="ctrl-btn original"></a><a href="#" class="ctrl-btn enlarge"></a><a href="#" class="ctrl-btn ' +
      'fullscreen"></a></div>').append('<div class="ctrl-name">' +
      'Screen Sharing from ' + getUserFromId(stream.origin)["userId"] + '</div>');
    $('#local-screen').remove();
    changeMode(MODES.LECTURE, !isLocalScreenSharing);
    streamObj["screen"] = stream;
    $('#screen-btn').addClass('disabled');

  } else {
    // append to global users
    var thisUser = getUserFromId(uid) || {};
    var htmlClass = isLocal ? 0 : (id - 1) % 5 + 1;
    thisUser.htmlId = id;
    thisUser.htmlClass = thisUser.htmlClass || htmlClass;
    thisUser.id = uid;

    // append new video to video panel
    var size  = getNextSize();
    $('#video-panel').append('<div class="' + size + ' clt-' + htmlClass +
      ' client pulse" ' + 'id="client-' + id + '"></div>') ;
    if (isLocal) {
      $('#client-' + id).append('<div class="self-arrow"></div>');
      $('#client-' + id).append('<video id="localVideo" playsinline muted autoplay style="position:relative"></video>')
      $('#localVideo').get(0).srcObject = stream.mediaStream;
    }else {
      $('#client-' + id).append('<video id="remoteVideo" playsinline autoplay style="position:relative"></video>')
      $('#remoteVideo').get(0).srcObject = stream.mediaStream;
    }

    var hasLeft = mode === MODES.GALAXY,
      element = $("#client-" + id),
      width = element.width(),
      height = element.height();
    element.find("video").css({
      width: hasLeft ? "calc(100% + " + (4 / 3 * height - width) + "px)" : "100%",
      height: "100%",
      top: "0px",
      left: hasLeft ? -(4 / 3 * height / 2 - width / 2) + "px" : "0px"
    });
    var player = $('#client-' + id).children(':not(.self-arrow)');
    player.attr('id', 'player-' + id).addClass('player')
      .css('background-color', 'inherit');
    player.children('div').remove();
    player.attr('id', 'player-' + id).addClass('video');

    // add avatar for no video users
    if (stream.mediaStream === false) {
      player.parent().addClass('novideo');
      player.append('<img src="img/avatar.png" class="img-novideo" />');
    }

    // control buttons and user name panel
    var resize = size === 'large' ? 'shrink' : 'enlarge';
    if (stream.source.video === 'mixed'){
      var name = "Mix Stream";
      $('#video-panel .largest').removeClass("largest");
      $("#client-" + id).addClass("largest");
    }else {
      var name = (stream === localStream) ? localName : getUserFromId(stream.origin).userId || {};
    }
    var muteBtn = "";

    if (stream.source.audio === 'mixed' && stream.source.video === 'mixed') {
      name = "MIX Stream";
      stream.hide = null;
      $("#client-" + id).attr("isMix", "true");
      document.getElementById("player-" + id).ondblclick = null;
      $("#client-" + id).find("video").attr("stream", "mix");
      $("#client-" + id).find("video").dblclick(function(e){
        if($('#video-' + id).attr("stream") === "mix"){
          var width = $('#video-' + id).width();
          var height = width*scaleLevel;
          var offset = ($('#video-' + id).height()-height)/2;
          var left = (e.offsetX/width).toFixed(3);
          var top = ((e.offsetY-offset)/height).toFixed(3);
          var streamId = getStreamId(left, top);
          if (streamId && streamObj[streamId]) {
            room.subscribe(streamObj[streamId], function() {
              console.info('subscribed:', streamId);
              $('#video-' + id).attr("src", streamObj[streamId].createObjectURL());
              $('#video-' + id).attr("stream", streamId);
              // stopMonitor();
              // monitor(streamObj[streamId]);
            }, function(err) {
              console.error(streamId, 'subscribe failed:', err);
            });
            stream.signalOnPauseAudio();
            stream.signalOnPauseVideo();
          }
        } else {
          var forward = streamObj[$('#video-' + id).attr("stream")];
          if (forward) {
            stream.signalOnPlayVideo();
            stream.signalOnPlayAudio();
            $('#video-' + id).attr("src", stream.createObjectURL());
            $('#video-' + id).attr("stream", "mix");
            // stopMonitor();
            // monitor(stream);
            room.unsubscribe(forward, function(et) {
              console.info(forward.id(), 'unsubscribe stream');
            }, function(err) {
              console.error(stream.id(), 'unsubscribe failed:', err);
            });
          }
        }
      });
      muteBtn = '<a href="#" class="ctrl-btn unmute"></a>';
      player.parent().append('<div id="pause-' + id +
        '" class="pause" style="display: none; width: 100%; height: auto; position: absolute; text-align: center; font: bold 30px Arial;">Paused</canvas>'
      );
    }
    streamIndices['client-' + id] = stream.id;

    $('#client-' + id).append('<div class="ctrl">' +
      '<a href="#" class="ctrl-btn ' + resize + '"></a>' +
      '<a href="#" class="ctrl-btn fullscreen"></a>' + muteBtn + '</div>')
      .append('<div class="ctrl-name">' + name + '</div>').append(
      "<div class='noCamera'></div>");
    stream.addEventListener('ended', function(event) {
      console.log("====stream ended:", stream.id);
      $('#client-' + stream.id).remove();
      if (stream.source.video === 'screen-cast') {
        $('#screen-btn').removeClass('disabled');
      }
    });
    relocate($('#client-' + id));
    changeMode(mode);
  }

  function mouseout(e) {
    isMouseDown = false;
    mouseX = null;
    mouseY = null;
    $(this).css('transition', '0.5s');
  }
  // no animation when dragging
  $('.client').mousedown(function(e) {
    isMouseDown = true;
    mouseX = e.clientX;
    mouseY = e.clientY;
    $(this).css('transition', '0s');
  }).mouseup(mouseout).mouseout(mouseout)
    .mousemove(function(e) {
      e.preventDefault();
      if (!isMouseDown || mouseX === null || mouseY === null || mode !==
        MODES.GALAXY) {
        return;
      }
      // update position to prevent from moving outside of video-panel
      var left = parseInt($(this).css('left')) + e.clientX - mouseX;
      var border = parseInt($(this).css('border-width')) * 2;
      var maxLeft = $('#video-panel').width() - $(this).width() - border;
      if (left < 0) {
        left = 0;
      } else if (left > maxLeft) {
        left = maxLeft;
      }
      $(this).css('left', left);

      var top = parseInt($(this).css('top')) + e.clientY - mouseY;
      var maxTop = $('#video-panel').height() - $(this).height() - border;
      if (top < 0) {
        top = 0;
      } else if (top > maxTop) {
        top = maxTop;
      }
      $(this).css('top', top);

      // update data for later calculation position
      $(this).data({
        left: left,
        top: top
      });
      mouseX = e.clientX;
      mouseY = e.clientY;
    });

  // stop pulse when animation completes
  setTimeout(function() {
    $('#client-' + id).removeClass('pulse');
  }, 800);
}

function changeMode(newMode, enlargeElement) {
  if (localStream) {
    console.log("localStream changeMode" + newMode);
  }
  switch (newMode) {
    case MODES.GALAXY:
      if ($('#galaxy-btn').hasClass('disabled')) {
        return;
      }
      mode = MODES.GALAXY;
      if (subscribeType === SUBSCRIBETYPES.FORWARD) {
        $('#galaxy-btn,#monitor-btn').removeClass('selected');
      } else {
        $('#galaxy-btn,#monitor-btn').addClass('disabled');
      }
      $('#galaxy-btn').addClass('selected');
      $('#video-panel').removeClass('monitor lecture')
        .addClass('galaxy');
      $.each($('.client'), function(key, value) {
        var d = smallRadius * 2;
        if ($(this).hasClass('large')) {
          d = largeRadius * 2;
          $(this).find('.enlarge')
            .removeClass('enlarge').addClass('shrink');
        } else {
          $(this).find('.shrink')
            .removeClass('shrink').addClass('enlarge');
        }
        var left = parseInt($(this).data('left'));
        if (left < 0) {
          left = 0;
        } else if (left > $('#video-panel').width() - d) {
          left = $('#video-panel') - d;
        }
        var top = parseInt($(this).data('top'));
        if (top < 0) {
          top = 0;
        } else if (top > $('#video-panel').height() - d) {
          top = $('#video-panel').height() - d;
        }
        $(this).css({
          left: left,
          top: top,
          width: d + 'px',
          height: d + 'px'
        }).data({
          left: left,
          top: top
        });
      });
      setTimeout(function() {
        $('.client').css("position", "absolute");
      }, 500);
      break;

    case MODES.MONITOR:
      if ($('#monitor-btn').hasClass('disabled')) {
        return;
      }
      mode = MODES.MONITOR;
      if (subscribeType === SUBSCRIBETYPES.FORWARD) {
        $('#galaxy-btn,#monitor-btn').removeClass('selected');
      } else {
        $('#galaxy-btn,#monitor-btn').addClass('disabled');
      }
      $('#monitor-btn').addClass('selected');
      $('#video-panel').removeClass('galaxy lecture')
        .addClass('monitor');
      $('.shrink').removeClass('shrink').addClass('enlarge');
      // updateMonitor();
      break;

    case MODES.LECTURE:
      if ($('#lecture-btn').hasClass('disabled')) {
        return;
      }
      mode = MODES.LECTURE;
      if (subscribeType === SUBSCRIBETYPES.FORWARD) {
        $('#galaxy-btn,#monitor-btn').removeClass('selected');
      } else {
        $('#galaxy-btn,#monitor-btn').addClass('disabled');
      }
      $('#lecture-btn').addClass('selected');
      $('#video-panel').removeClass('galaxy monitor')
        .addClass('lecture');
      $('.shrink').removeClass('shrink').addClass('enlarge');
      if (typeof enlargeElement !== 'boolean') {
        var largest = enlargeElement || ($('#screen').length > 0 ? $('#screen') :
          ($('.largest').length > 0 ? $('.largest').first() : ($('.large').length >
          0 ? $('.large').first() : $('.client').first())));
        $('.client').removeClass('largest');
        largest.addClass('largest')
          .find('.enlarge').removeClass('enlarge').addClass('shrink');
      }
      updateLecture(enlargeElement);
      break;

    default:
      console.error('Illegal mode name');
  }
  //TODO: limit https
  if (window.location.protocol !== "https:") {
    // $("#screen-btn").addClass("disabled");
  }

  // update canvas size in all video panels
  $('.player').trigger('resizeVideo');
  setTimeout(resizeStream, 500, newMode);
}
*/
