import {Component, ElementRef, OnDestroy, OnInit, QueryList, Renderer2, ViewChild, ViewChildren} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MeetingService} from '../meeting.service';
import {OwtService} from '../owt.service';
import {__await} from 'tslib';
import {Config} from '../../services/config';
import {ToastrService} from 'ngx-toastr';

// import * as $ from 'jquery';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  // styleUrls: ['./dashboard.component.css']
  styleUrls: ['../../../assets/css/style.css']
})
export class DashboardComponent implements OnInit, OnDestroy {

  meetingForm: FormGroup;
  joinMeetingForm: FormGroup;
  roomId: any;
  meetingCode: any;
  meetingUrl: any;
  currentMeeting: any;
  currentAttendee: any;
  localID: any;
  captchaData: any;
  captchaVerified = false;

  constructor(private router: Router,
              private meetingService: MeetingService,
              private owtService: OwtService,
              private fb: FormBuilder,
              private elementRef: ElementRef,
              private activatedRoute: ActivatedRoute,
              private renderer: Renderer2,
              public toasterService: ToastrService,
              private config: Config) {
    this.captchaVerified = false;
    this.createMeetingFormGroup();
    this.activatedRoute.queryParams.subscribe(params => {
      this.roomId = params['room_id'];
      this.meetingCode = params['meeting_code'];
    });
    this.joinMeetingFormGroup();
  }

  @ViewChildren('recaptcha') public recaptchaElement: QueryList<ElementRef>;

  async ngOnInit() {
    this.currentMeeting = await JSON.parse(localStorage.getItem('currentMeeting'));
    this.currentAttendee = await JSON.parse(localStorage.getItem('currentAttendee'));
    this.localID = await JSON.parse(localStorage.getItem('localID'));
    this.addRecaptchaScript();
  }

  createMeetingFormGroup() {
    this.meetingForm = this.fb.group({
      host_name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(250)]],
      display_name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(250)]],
    });
  }

  async createMeeting() {
    if (this.currentMeeting !== null && this.currentAttendee !== null) {
      await this.endLastMeetingStorage();
    }

    let isValid = this.meetingForm.valid;
    /*let meeting = this.meetingForm.value;
    meeting.name = Math.random().toString(36).substring(2, 8) + Math.random().toString(36).substring(2, 8);
    meeting.meeting_code = Math.random().toString(36).substring(2, 5) + Math.random().toString(36).substring(2, 5);
  const displayName = meeting.display_name;
  delete meeting.meeting_display_name;
  console.log(meeting, isValid);*/
    if (this.captchaVerified === false) {
      isValid = false;
    }

    if (isValid) {
      try {
        let meeting = this.meetingForm.value;
        meeting.name = Math.random().toString(36).substring(2, 8) + Math.random().toString(36).substring(2, 8);
        meeting.meeting_code = Math.random().toString(36).substring(2, 5) + Math.random().toString(36).substring(2, 5);
        const displayName = meeting.display_name;
        delete meeting.meeting_display_name;
        let meetings = await this.meetingService.index({name: meeting.name});
        if (meetings.length > 0) {
          this.toasterService.error('Meeting Room already exist with this name.');
        } else {
          let owtRoom = await this.owtService.createRoom(meeting.name);
          console.log('OWT Room ', owtRoom);
          meeting.owt_meeting_id = owtRoom.id;
          meeting.display_name = displayName;
          console.log('Meeting Data ', meeting);
          let meetingRoom = await this.meetingService.createMeeting(meeting);
          if (meetingRoom) {
            console.log('Meeting Room ', meetingRoom);
            await localStorage.setItem('currentMeeting', JSON.stringify(meetingRoom));
            /*const currentAttendee = await this.meetingService.addMeetingAttendee(meetingRoom._id, {
              name: meeting.host_name,
              role: 'host'
            });*/
            await localStorage.setItem('currentAttendee', JSON.stringify({
              name: meeting.host_name,
              role: 'host'
            }));
            this.meetingUrl = window.location.protocol + '//' + window.location.host + '/#/meeting?room_id=';
            this.meetingUrl = encodeURI(this.meetingUrl + meeting.name + '&meeting_code=' + meeting.meeting_code);
            localStorage.setItem('meetingDetails', JSON.stringify({
              /*name: meeting.name,
              displayName: meeting.display_name,
              hostName: meeting.host_name,
              meetingCode: meeting.meeting_code,*/
              meetingUrl: this.meetingUrl
            }));
            await this.router.navigate(['/meeting', 'waiting-room']);
          }
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      this.toasterService.error('Please fill all the fields.');
    }
  }

  joinMeetingFormGroup() {
    if (this.roomId && this.meetingCode) {
      this.joinMeetingForm = this.fb.group({
        attendee_name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(250)]],
      });
    } else {
      this.joinMeetingForm = this.fb.group({
        attendee_name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(250)]],
        name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(250)]],
        meeting_code: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(250)]],
      });
    }

    /*this.joinMeetingForm.patchValue({
      name: this.roomId,
      meeting_code: this.meetingCode,
      });*/
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.body, 'modal-open');
    let modalChild = document.querySelector('.modal-backdrop');
    if (modalChild) {
      this.renderer.removeChild(document.body, modalChild);
    }
  }

  async joinMeeting() {
    if (this.currentMeeting !== null && this.currentAttendee !== null) {
      await this.endLastMeetingStorage();
    }
    let captchaResponse = window['grecaptcha'].getResponse(this['widgetId' + 2]);

    const meeting = this.joinMeetingForm.value;
    if (this.roomId && this.meetingCode) {
      meeting.name = this.roomId;
      meeting.meeting_code = this.meetingCode;
    }
    let isValid = this.joinMeetingForm.valid;
    const attendeeName = meeting.attendee_name;
    delete meeting.attendee_name;
    console.log(meeting, isValid);
    if (this.captchaVerified === false && captchaResponse) {
      isValid = false;
    }

    if (isValid) {
      try {
        let response = await this.meetingService.validateMeeting(meeting);
        const bannedFromMeetings = await JSON.parse(localStorage.getItem('bannedFromMeetings'));
        let isBanned = false;
        if (Array.isArray(bannedFromMeetings) && bannedFromMeetings.length > 0) {
          if (bannedFromMeetings.includes(meeting.name)) {
            isBanned = true;
          }
        }
        if (response.data !== null) {
          /*const currentAttendee = await this.meetingService.addMeetingAttendee(response.data._id, {
            name: attendeeName,
            role: 'attendee'
          });*/

          if (isBanned) {
            this.toasterService.error('You are banned from this meeting.');
          } else {
            await localStorage.setItem('currentMeeting', JSON.stringify(response.data));
            await localStorage.setItem('currentAttendee', JSON.stringify({
              name: attendeeName,
              role: 'attendee'
            }));
            this.meetingUrl = window.location.protocol + '//' + window.location.host + '/#/meeting?room_id=';
            this.meetingUrl = encodeURI(this.meetingUrl + response.data.name + '&meeting_code=' + response.data.meeting_code);
            localStorage.setItem('meetingDetails', JSON.stringify({meetingUrl: this.meetingUrl}));
            await this.router.navigate(['/meeting', 'waiting-room']);
          }
        } else {
          this.toasterService.error('You have entered invalid credentials.');
          if (isBanned) {
            bannedFromMeetings.splice(bannedFromMeetings.indexOf(meeting.name), 1);
            await localStorage.setItem('bannedFromMeetings', JSON.stringify(bannedFromMeetings));
          }

        }
      } catch (error) {
        console.log(error);
      }
    } else {
      this.toasterService.info('Please fill all the fields.');
    }
  }

  async fillFeedback() {
    await localStorage.removeItem('feedbackCounter');
    await localStorage.setItem('feedbackCounter', '0');
    return await this.router.navigate(['/meeting', 'feedback']);
  }

  async endLastMeetingStorage() {
    if (this.localID !== null) {
      await this.owtService.deleteParticipant(this.currentMeeting.owt_meeting_id, this.localID);
    }
    if (this.currentMeeting._id !== undefined && this.currentAttendee._id !== undefined && this.currentMeeting.owt_meeting_id !== undefined) {
      await this.meetingService.leaveMeeting(this.currentMeeting._id, this.currentAttendee._id);
      await this.owtService.getParticipants(this.currentMeeting.owt_meeting_id).then(async (participants) => {
        if (participants === []) {
          await this.owtService.deleteRoom(this.currentMeeting.owt_meeting_id);
          await this.meetingService.endMeeting(this.currentMeeting._id);
        }
      }).catch((err) => {
        console.log(err);
      });
      await localStorage.clear();
      await localStorage.setItem('lastMeeting', JSON.stringify({meeting: this.currentMeeting, attendee: this.currentAttendee}));
      await localStorage.setItem('feedbackCounter', '1');
    }
  }

  async captchaStatus() {
    this.captchaVerified = false;
    let count = 1;
    this.recaptchaElement.forEach(el => {
      window['grecaptcha'].reset(this['widgetId' + count]);
      count = count + 1;
    });
  }

  addRecaptchaScript() {
    window['grecaptchaCallback'] = () => {
      this.renderReCaptcha();
    };

    (function (d, s, id, obj) {
      let js;
      const fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        obj.renderReCaptcha();
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = 'https://www.google.com/recaptcha/api.js?onload=grecaptchaCallback&amp;render=explicit';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'recaptcha-jssdk', this));

  }

  renderReCaptcha() {
    let count = 1;

    this.recaptchaElement.forEach(el => {
      this['widgetId' + count] = window['grecaptcha'].render(el.nativeElement, {
        sitekey: this.config.siteKey,
        callback: async (response) => {
          const captchaData = {
            token: response
          };
          const result = await this.meetingService.verifyCaptcha(captchaData);
          if (result.google_response.success) {
            this.captchaVerified = true;
          }
        },
        'expired-callback': async (response) => {
          this.captchaVerified = false;
        },
        'error-callback': async (response) => {
          console.log('Something went wrong. Please try again.');
        }
      });
      count = count + 1;
    });
  }

}
