import {Injectable} from '@angular/core';
import {RequestService} from '../services/request.service';
import {Config} from '../services/config';

@Injectable({
  providedIn: 'root'
})

export class OwtService {

  baseUrl: string = this.config.owtUrl + '/api/owt/';

  constructor(private requestService: RequestService, public config: Config) {
  }

  mixStream(room, stream, view) {
    const jsonPatch = [{
      op: 'add',
      path: '/info/inViews',
      value: view
    }];
    this.requestService.send('PATCH', this.baseUrl + room + '/streams/' + stream, jsonPatch, this.onResponse);
  }

  startStreamingIn(room, inUrl) {
    const options = {
      url: inUrl,
      media: {
        audio: 'auto',
        video: true
      },
      transport: {
        protocol: 'udp',
        bufferSize: 2048
      }
    };
    this.requestService.send('POST', this.baseUrl + room + '/streaming-ins', options, this.onResponse);
  }

  createToken(room, user, role) {
    const body = {
      preference: {isp: 'isp', region: 'region'},
      user: user,
      role: role
    };
    if (room) {
      this.requestService.send('POST', this.baseUrl + room + '/tokens/', body, this.onResponse);
    }
    /*else {
      this.requestService.send('POST', this.baseUrl + '/createToken/', body, callback);
    }*/
  }

  async createRoom(name, options?) {
    const body = {
      name: name,
      options: options ? options : {}
    };
    return await this.requestService.request('post', this.baseUrl, body);
  }

  async deleteRoom(roomID) {
    return await this.requestService.request('delete', this.baseUrl + roomID);
  }

  async deleteParticipant(roomID, participantID) {
    return await this.requestService.request('DELETE', this.baseUrl + roomID + '/participants/' + participantID);
  }

  async getParticipants(roomID) {
    return await this.requestService.request('GET', this.baseUrl + roomID + '/participants');
  }

  async getStreams(room) {
    return await this.requestService.request('GET', this.baseUrl + room + '/streams/');
  }

  async dropStream(room, stream) {
    return await this.requestService.request('DELETE', this.baseUrl + room + '/streams/' + stream);
  }

  async getStreamInfo(room, stream) {
    return await this.requestService.request('GET', this.baseUrl + room + '/streams/' + stream);
  }

  async updateParticipantPermissions(room, participant, permissions) {
    const body = [
      {
        op: 'replace',
        path: '/permission/publish',
        value: permissions
      }
    ];
    return await this.requestService.request('PATCH', this.baseUrl + room + '/participants/' + participant, body);
  }

  async pauseStream(room, stream, track) {
    let jsonPatch = [];
    if (track === 'audio' || track === 'av') {
      jsonPatch.push({
        op: 'replace',
        path: '/media/audio/status',
        value: 'inactive'
      });
    }

    if (track === 'video' || track === 'av') {
      jsonPatch.push({
        op: 'replace',
        path: '/media/video/status',
        value: 'inactive'
      });
    }
    return await this.requestService.request('PATCH', this.baseUrl + room + '/streams/' + stream, jsonPatch);
  }

  async playStream(room, stream, track) {
    let jsonPatch = [];
    if (track === 'audio' || track === 'av') {
      jsonPatch.push({
        op: 'replace',
        path: '/media/audio/status',
        value: 'active'
      });
    }

    if (track === 'video' || track === 'av') {
      jsonPatch.push({
        op: 'replace',
        path: '/media/video/status',
        value: 'active'
      });
    }
    return await this.requestService.request('PATCH', this.baseUrl + room + '/streams/' + stream, jsonPatch);
  }

  onResponse(result) {
    if (result) {
      try {
        return {response: JSON.parse(result)};
      } catch (e) {
        return {response: result};
      }
    } else {
      return {response: null};
    }
  }

}
