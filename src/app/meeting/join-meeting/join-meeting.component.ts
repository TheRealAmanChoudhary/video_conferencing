import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MeetingService} from '../meeting.service';

@Component({
  selector: 'app-join-meeting',
  templateUrl: './join-meeting.component.html',
  styleUrls: ['./join-meeting.component.css']
})
export class JoinMeetingComponent implements OnInit {

  joinMeetingForm: FormGroup;
  roomId: any;
  meetingCode: any;

  constructor(private router: Router, private meetingService: MeetingService, private fb: FormBuilder, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.roomId = params['room_id'];
      this.meetingCode = params['meeting_code'];
      });
    this.joinMeetingFormGroup();
  }

  async ngOnInit() {
    const currentMeeting = await JSON.parse(localStorage.getItem('currentMeeting'));
    const attendeeData = await JSON.parse(localStorage.getItem('attendeeData'));
    const currentAttendee = await JSON.parse(localStorage.getItem('currentAttendee'));
    const lastMeeting = await JSON.parse(localStorage.getItem('lastMeeting'));

    if (currentMeeting !== null && attendeeData !== null && currentAttendee === null) {
      await this.router.navigate(['/meeting', 'waiting-room']);
    } else if (currentMeeting !== null && attendeeData !== null && currentAttendee !== null) {
      await this.router.navigate(['/meeting', 'meeting-room']);
    } else if (lastMeeting !== null) {
      await this.router.navigate(['/meeting', 'feedback']);
    }
    //await localStorage.clear();
  }

  joinMeetingFormGroup() {
    this.joinMeetingForm = this.fb.group({
      attendee_name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(250)]],
      name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(250)]],
      meeting_code: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(250)]],
    });

    this.joinMeetingForm.patchValue({
      name: this.roomId,
      meeting_code: this.meetingCode,
  });
  }

  async joinMeeting() {
    const meeting = this.joinMeetingForm.value;
    const isValid = this.joinMeetingForm.valid;
    const attendeeName = meeting.attendee_name;
    delete meeting.attendee_name;
    console.log(meeting, isValid);

    if (isValid) {
      try {
        let response = await this.meetingService.validateMeeting(meeting);
        if (response.data !== null) {
          localStorage.setItem('currentMeeting', JSON.stringify(response.data));
          localStorage.setItem('attendeeData', JSON.stringify({name: attendeeName, role: 'attendee'}));
          await this.router.navigate(['/meeting', 'waiting-room']);
        } else {
          alert('You have entered invalid credentials!');
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      alert('Please fill all the fields!');
    }
  }

}
