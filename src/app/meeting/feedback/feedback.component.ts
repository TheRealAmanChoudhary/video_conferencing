import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MeetingService} from '../meeting.service';
import * as $ from 'jquery';
import {OwtService} from '../owt.service';

@Component({
    selector: 'app-feedback',
    templateUrl: './feedback.component.html',
    styleUrls: ['../../../assets/css/login/login.css', '../../../assets/css/login/checkAV.css']
})
export class FeedbackComponent implements OnInit, OnDestroy {

    feedbackForm: FormGroup;
    rating: number = 0;
    lastMeeting: any = null;
    public collection = [];

    constructor(private router: Router, private meetingService: MeetingService, private owtService: OwtService, private fb: FormBuilder) {
        this.createFeedbackFormGroup();
    }

    async ngOnInit() {
        const meetingInProgress = await localStorage.getItem('meetingInProgress');
        const currentMeeting = await JSON.parse(localStorage.getItem('currentMeeting'));
        const currentAttendee = await JSON.parse(localStorage.getItem('currentAttendee'));
        const waiting = await localStorage.getItem('waiting');
        const localID = await JSON.parse(localStorage.getItem('localID'));
        this.lastMeeting = await JSON.parse(localStorage.getItem('lastMeeting'));
        const feedbackCounter = await localStorage.getItem('feedbackCounter');

        if (meetingInProgress !== null && meetingInProgress === '1') {
            await this.router.navigate(['meeting-in-progress']);
        } else if (currentMeeting !== null && currentAttendee !== null && waiting === '1') {
            if (localID !== null) {
                await this.owtService.deleteParticipant(currentMeeting.owt_meeting_id, localID);
            }
            await this.router.navigate(['/meeting', 'meeting-room']);
        } else if (this.lastMeeting === null || feedbackCounter === null || feedbackCounter === '1') {
            await this.router.navigate(['/meeting']);
        }
        await localStorage.removeItem('feedbackCounter');
        await localStorage.setItem('feedbackCounter', '1');

        $(document).ready(function () {
            let checkboxes = $('.rating li input[type=\'radio\']'),
                actions = $('.actions'),
                dismiss = $('.dismiss');
            checkboxes.click(function () {
                actions.css('display', 'block', !checkboxes.is(':checked'));
                dismiss.css('display', 'none');
            });
        });

    }

    async ngOnDestroy() {
    }

    createFeedbackFormGroup() {
        this.feedbackForm = this.fb.group({
            feedback: ['', []],
            name: ['', []],
            email: ['', []],
            phoneNo: ['', []],
            anonymousCheck: ['', []],
        });
    }

    addRating(rating: number) {
        this.rating = rating;
    }

    feedbackDetails() {
        if ($('#anonymousCheck').is(':checked')) {
            $('.feedback-details').css('display', 'none');
        } else {
            $('.feedback-details').css('display', 'block');
        }
    }

    async submitFeedback() {
        const feedback = this.feedbackForm.value;
        const isValid = this.feedbackForm.valid;
        console.log(feedback, isValid);

        if (isValid) {
            if (this.rating !== null) {
                feedback.rating = this.rating;
            }
            try {
                let response = await this.meetingService.updateAttendeeFeedback(this.lastMeeting.meeting._id, this.lastMeeting.attendee._id, feedback);
                if (response) {
                    localStorage.removeItem('lastMeeting');
                    localStorage.removeItem('feedbackCounter');
                    await this.router.navigate(['/meeting']);
                }
            } catch (error) {
                console.log(error);
            }
        }
    }

    async dismissFeedback() {
        localStorage.removeItem('lastMeeting');
        localStorage.removeItem('feedbackCounter');
        await this.router.navigate(['/meeting']);
    }

    onlyNumberKey(event): boolean {
    let patt = /^([0-9])$/;
    let result = patt.test(event.key);
    return result;
  }

}
