import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
//import {environment} from '../../environments/environment';
import {Observable, Subject} from 'rxjs';

@Injectable()
export class WebsocketService {

  // Our socket connection
  private socket;

  constructor() {
  }

  connect(eventListner): Subject<MessageEvent> {
    console.log('eventListner', eventListner);
    this.socket = io('http://localhost:4040');//, {transports: ['websocket'], upgrade: false});

    // We define our observable which will observe any incoming messages
    // from our socket.io server.
    const observable = new Observable(observer => {
      this.socket.on(eventListner, (data) => {
        console.log('Received message from Websocket Server: ' + eventListner);
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });

    const observer = {
      next: (data: Object) => {
        console.log(data);
        this.socket.emit(eventListner, JSON.stringify(data));
      },
    };

    // we return our Rx.Subject which is a combination
    // of both an observer and observable.
    return Subject.create(observer, observable);
  }

}
