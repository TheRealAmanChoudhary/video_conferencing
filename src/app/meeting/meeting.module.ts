import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard/dashboard.component';
import {WaitingRoomComponent} from './waiting-room/waiting-room.component';
import {MeetingRoomComponent} from './meeting-room/meeting-room.component';
import {MeetingRoutingModule} from './meeting-routing.module';
import {CreateMeetingComponent} from './create-meeting/create-meeting.component';
import {JoinMeetingComponent} from './join-meeting/join-meeting.component';
import {FeedbackComponent} from './feedback/feedback.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {CopyClipboardDirective} from '../directives/copy-clipboard.directive';


@NgModule({
  declarations: [DashboardComponent,
    WaitingRoomComponent,
    MeetingRoomComponent,
    CreateMeetingComponent,
    JoinMeetingComponent,
    FeedbackComponent, CopyClipboardDirective],
  imports: [
    CommonModule,
    MeetingRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ]
})
export class MeetingModule {
}
