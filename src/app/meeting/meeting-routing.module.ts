import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {WaitingRoomComponent} from './waiting-room/waiting-room.component';
import {MeetingRoomComponent} from './meeting-room/meeting-room.component';
import {CreateMeetingComponent} from './create-meeting/create-meeting.component';
import {JoinMeetingComponent} from './join-meeting/join-meeting.component';
import {FeedbackComponent} from './feedback/feedback.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: DashboardComponent,
        data: {
          title: 'Dashboard'
        }
      },
      {
        path: 'waiting-room',
        component: WaitingRoomComponent,
        data: {
          title: 'Waiting room'
        }
      },
      {
        path: 'meeting-room',
        component: MeetingRoomComponent,
        data: {
          title: 'Meeting room'
        }
      },
     /* {
        path: 'create',
        component: CreateMeetingComponent,
        data: {
          title: 'Create meeting'
        }
      },
      {
        path: 'join',
        component: JoinMeetingComponent,
        data: {
          title: 'Join meeting'
        }
      },*/
      {
        path: 'feedback',
        component: FeedbackComponent,
        data: {
          title: 'Meeting feedback'
        }
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MeetingRoutingModule {
}
