import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  QueryList,
  Renderer2,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import {
  faChevronUp,
  faMicrophone,
  faMicrophoneSlash, faSignOutAlt,
  faUsers,
  faVideo,
  faVideoSlash,
  faVolumeMute,
  faVolumeUp
} from '@fortawesome/free-solid-svg-icons';
import {Router} from '@angular/router';
import {MeetingService} from '../meeting.service';
import * as Owt from '../../../scripts/owt';
import {HelperService} from '../../services/helper.service';
import {OwtService} from '../owt.service';
import {interval, timer} from 'rxjs';

import * as $ from 'jquery';
// import {ToastrService} from 'ngx-toastr';
import {timeout} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {Config} from '../../services/config';

@Component({
  selector: 'app-meeting-room',
  templateUrl: './meeting-room.component.html',
  styleUrls: ['../../../assets/css/style.css',
    './meeting-room.component.css'
  ]
})
export class MeetingRoomComponent implements OnInit, OnDestroy {
  @ViewChild('localVideoStream', {static: false}) localVideoStream: ElementRef;
  @ViewChild('screenStream', {static: false}) screenStream: ElementRef;
  /*screenStream: ElementRef;
  @ViewChild('screenStream', {static: false}) set content(content: ElementRef) {
    if(content) { // initially setter gets called with undefined
      this.screenStream = content;
    }
  }*/
  // @ViewChild('remoteScreenStream', {static: false}) public remoteScreenStream: ElementRef;
  @ViewChildren('videos') public videoStreamDiv: QueryList<ElementRef>;
  @ViewChild('audioSource', {static: false}) audioSource: ElementRef;
  @ViewChild('videoSource', {static: false}) videoSource: ElementRef;

  objectKeys = Object.keys;

  private speakingStreak = {};

  public attendeePinned = false;
  public activeRenameID = null;
  public kickOutMessage = 'Your meeting has ended.';
  public screenLayout = 'GRID';
  public selectedLayoutOption = 'GRID';
  public audioStatus = true;
  public videoStatus = true;
  public speakerStatus = true;
  public showAttendees = false;
  public localStreamLoaded = false;
  public mediaStream;
  public cameraMediaStream;
  public sources: any = {
    audioIn: [],
    audioOut: [],
    video: []
  };

  public mediaStatus: any = {
    audio: true,
    video: true
  };

  public meetingTimer = '--:--';

  public icons = {
    mute: faMicrophone,
    unmute: faMicrophoneSlash,
    speaker_on: faVolumeUp,
    speaker_off: faVolumeMute,
    video_on: faVideo,
    video_off: faVideoSlash,
    attendees: faUsers,
    chevron_up: faChevronUp,
    exit: faSignOutAlt
  };

  private room;
  private roomId;
  private localStream = null;
  private localPublication;
  private localScreen = null;
  private localScreenPublication;
  private remoteScreenStream = null;
  private SUBSCRIBETYPES = {
    FORWARD: 'forward',
    MIX: 'mix'
  };
  private subscribeType = this.SUBSCRIBETYPES.FORWARD;
  private remoteMixedSub;
  private screenSub;
  private modeName = 'Forward stream';
  isScreenSharing = false;
  isLocalScreenSharing = false;
  private scaleLevel = 3 / 4;
  private token;
  attendees = {};
  public attendeeCount = '1';
  selectedDevice = {
    audio: null,
    video: null,
    audioDestination: null
  };
  private selectedResolution = new Owt.Base.Resolution(640, 360);
  private mediaConstraints;
  private mediaPermissions;
  avTrackConstraint = {
    audio: {
      source: 'mic', deviceId: undefined
    },
    video: {
      resolution: this.selectedResolution,
      frameRate: 24,
      source: 'camera',
      deviceId: undefined
    },
  };
  streamData = {};
  public attendeeList = [];
  public activeAttendeeID = null;
  private streamMap = new Map();
  private attendeesMap = new Map();
  localID;
  private localScreenId;
  currentMeeting: any = null;
  currentAttendee: any = null;
  meetingDetails: any = null;
  private meetingUrl: any = null;
  private attendeeRole: any = null;
  private timerSubscription;
  private speakingIntervalSubscription = {};
  private speakingSubscription: any;
  public joinedAttendees;
  private currentMediaConstraints: any;

// view variables
  public sideBarStatus = false;
  @ViewChild('participantTab', {static: false}) participantTab: ElementRef;
  @ViewChild('inviteTab', {static: false}) inviteTab: ElementRef;
  @ViewChild('participantLink', {static: false}) participantLink: ElementRef;
  @ViewChild('inviteLink', {static: false}) inviteLink: ElementRef;

  @ViewChild('copyButton', {static: false}) copyButton: ElementRef;
  @ViewChild('meetingEndedModal', {static: false}) meetingEndedModal: ElementRef;
  @ViewChild('popUp', {static: false}) popUp: ElementRef;

//
  public speaking = false;
  public instant: any;
  public microphone: any;
  public javascriptNode: any;
  public audio: any;
  public cameraError = false;
  public localMediaCreationError = false;
  screenShare = 0;
  screenWhenAlreadySharing = 0;
  screenShareSettings: any;
  displayScreenShare = true;
  attendeesCount: Number;

  @ViewChild('videoPreview', {static: false}) videoPreview: ElementRef;
  @ViewChild('videoSettingsPreview', {static: false}) videoSettingsPreview: ElementRef;

  @HostListener('window:beforeunload', ['$event'])
  public async onBeforeUnload($event) {
    // return;//todo
    await localStorage.removeItem('meetingInProgress');
    this.currentMediaConstraints = await JSON.parse(localStorage.getItem('mediaConstraints'));
    const mediaConstraints = {
      audio: {
        status: this.audioStatus,
        // todo: change source value to current source value when device settings is added in meeting room
        source: this.currentMediaConstraints.audio.source,
        destination: this.currentMediaConstraints.audio.destination
      },
      video: {
        status: this.videoStatus,
        source: this.currentMediaConstraints.video.source
      }
    };
    await localStorage.setItem('mediaConstraints', JSON.stringify(mediaConstraints));
    await localStorage.removeItem('localID');
    if (this.room !== undefined) {
      await this.room.leave();
    }
  }

  @HostListener('window:popstate', ['$event'])
  public async onPopState($event) {
    /*if (this.room) {
      await this.room.clearEventListener('serverdisconnected');
    }*/
    await localStorage.removeItem('meetingInProgress');
    // todo: user doesn't leave room when pressing back button and a new stream gets added
    // await this.room.leave();
  }

  constructor(private renderer: Renderer2,
              private elementRef: ElementRef,
              private router: Router,
              private meetingService: MeetingService,
              private owtService: OwtService,
              public helperService: HelperService,
              public toasterService: ToastrService,
              private config: Config,
              private changeDetector: ChangeDetectorRef) {
  }

  async ngOnInit() {
    await this.loadScript();
    const meetingInProgress = await localStorage.getItem('meetingInProgress');
    this.currentMeeting = await JSON.parse(localStorage.getItem('currentMeeting'));
    this.currentAttendee = await JSON.parse(localStorage.getItem('currentAttendee'));
    this.meetingDetails = await JSON.parse(localStorage.getItem('meetingDetails'));
    this.screenShareSettings = await JSON.parse(localStorage.getItem('screenShare'));
    const waiting = await localStorage.getItem('waiting');
    if (this.meetingDetails) {
      this.meetingUrl = this.meetingDetails.meetingUrl;
    }
    if (this.currentAttendee) {
      this.attendeeRole = this.currentAttendee.role;
    }
    this.mediaConstraints = await JSON.parse(localStorage.getItem('mediaConstraints'));
    this.mediaPermissions = await JSON.parse(localStorage.getItem('mediaPermissions'));
    this.sources = await JSON.parse(localStorage.getItem('devices'));

    if (!!this.mediaConstraints) {
      this.selectedDevice.audio = this.mediaConstraints.audio.source;
      this.selectedDevice.video = this.mediaConstraints.video.source;
      this.selectedDevice.audioDestination = this.mediaConstraints.audio.destination;

      // @ts-ignore
      this.avTrackConstraint.audio.deviceId = this.selectedDevice.audio;

      if (this.mediaConstraints.video.status) {
        // @ts-ignore
        this.avTrackConstraint.video.deviceId = this.selectedDevice.video;
        if (this.mediaConstraints.video.resolution) {
          // @ts-ignore
          this.selectedResolution = new Owt.Base.Resolution(this.mediaConstraints.video.resolution.width, this.mediaConstraints.video.resolution.height);
          this.avTrackConstraint.video.resolution = this.selectedResolution;
        }
      } else {
        // @ts-ignore
        this.avTrackConstraint.video = false;
      }

      this.audioStatus = !!this.mediaConstraints.audio.status;
      this.videoStatus = !!this.mediaConstraints.video.status;
    }
    console.log(this.selectedResolution);

    if (this.screenShareSettings === null) {
      this.screenShareSettings = {
        screenShare: this.screenShare,
        screenWhenAlreadySharing: this.screenWhenAlreadySharing
      };
      await localStorage.setItem('screenShare', JSON.stringify(this.screenShareSettings));
    } else {
      this.screenShare = this.screenShareSettings.screenShare;
      this.screenWhenAlreadySharing = this.screenShareSettings.screenWhenAlreadySharing;
    }

    // console.log(this.currentMeeting, this.currentAttendee, meetingInProgress);
    if (meetingInProgress !== null && meetingInProgress === '1') {
      await this.router.navigate(['meeting-in-progress']);
    } else if (this.currentMeeting === null || this.currentAttendee === null) {
      await this.router.navigate(['/meeting']);
      // } else if (this.currentMeeting !== null && this.currentAttendee !== null && waiting === null) {
    } else if (waiting === null) {
      await this.router.navigate(['/meeting', 'waiting-room']);
    } else {
      await localStorage.setItem('meetingInProgress', '1');
      this.roomId = this.currentMeeting.owt_meeting_id;
      const body = {
        preference: {
          isp: 'isp',
          region: 'region'
        },
        user: this.currentAttendee._id,
        role: 'presenter'
      };

      await this.meetingService.createToken(this.roomId, body).subscribe(async res => {
        this.token = res.token;
        await this.roomInit();
        await this.joinMeeting();
        if (Object.keys(this.attendees).length === 1) {
          this.currentMeeting = await this.meetingService.startMeeting(this.currentMeeting._id);
          await localStorage.setItem('currentMeeting', JSON.stringify(this.currentMeeting));
        }
        if (this.currentMeeting.started_at === undefined) {
          this.currentMeeting = await this.meetingService.getMeeting(this.currentMeeting._id);
          await localStorage.setItem('currentMeeting', JSON.stringify(this.currentMeeting));
        }
        // @ts-ignore
        const timeDifference = await Math.abs(new Date(this.currentMeeting.started_at) - new Date());
        this.meetingTimer = await this.helperService.msToTime(timeDifference);
        const timer$ = timer(0, 1000);
        this.timerSubscription = await timer$.subscribe(counter => {
          this.meetingTimer = this.helperService.msToTime(timeDifference + (counter * 1000));
        });
      });
    }
  }

  loadScript() {

// todo
    /*$("#volume, #volume1").slider({
      min: 0,
      max: 100,
      value: 0,
      range: "min",
      slide: function(event, ui) {
        setVolume(ui.value / 100);
      }
    });
    function setVolume(myVolume) {
      var myMedia = document.getElementById('myMedia');
      //myMedia.volume = myVolume;
    }
    var myMedia = document.createElement('audio');
    $('#ValumeBx, #ValumeBx1').append(myMedia);
    myMedia.id = 'myMedia';*/

    this.elementRef.nativeElement.querySelectorAll('.FooterNav .dropdown-menu a.dropdown-toggle').forEach(el => {
      el.addEventListener('click', event => {
        event.stopPropagation();
        const menuOption = event.target;
        const subMenu = this.renderer.nextSibling(menuOption);

        this.elementRef.nativeElement.querySelectorAll('.FooterNav .dropdown-menu .dropdown-menu').forEach(elem => {
          if (elem === subMenu) {
            if (elem.classList.contains('show')) {
              this.renderer.removeClass(elem, 'show');
            } else {
              this.renderer.addClass(elem, 'show');
            }
          } else {
            this.renderer.removeClass(elem, 'show');
          }
        });

      });
    });


    this.elementRef.nativeElement.querySelectorAll('.DD-Arrow').forEach(el => {
      el.addEventListener('click', (event) => {
        const menuOption = event.target.parentElement.previousSibling;
        this.elementRef.nativeElement.querySelectorAll('.FooterNav .dropdown-menu .dropdown-menu').forEach(elem => {
          this.renderer.removeClass(elem, 'show');
        });
      });
    });
  }


  async toggleSpeaker() {
    await this.test();
    if (this.speakerStatus) {
      await this.videoStreamDiv.forEach(el => {
        el.nativeElement.volume = 0;
      });
      this.speakerStatus = false;
    } else {
      await this.videoStreamDiv.forEach(el => {
        el.nativeElement.volume = 1;
      });
      this.speakerStatus = true;
    }
  }

  async leaveMeeting(remove = 0) {
    await this.stopStreams();
    if (this.room !== undefined) {
      await this.room.leave();
    }
    await this.meetingService.leaveMeeting(this.currentMeeting._id, this.currentAttendee._id);
    if (Object.keys(this.attendees).length === 1) {
      await this.owtService.deleteRoom(this.currentMeeting.owt_meeting_id);
      await this.meetingService.endMeeting(this.currentMeeting._id);
    }

    const bannedFromMeetings = await JSON.parse(localStorage.getItem('bannedFromMeetings'));
    await localStorage.clear();
    await localStorage.setItem('lastMeeting', JSON.stringify({meeting: this.currentMeeting, attendee: this.currentAttendee}));
    await localStorage.setItem('feedbackCounter', '0');
    if (bannedFromMeetings !== null) {
      await localStorage.setItem('bannedFromMeetings', JSON.stringify(bannedFromMeetings));
    }
    if (remove === 0) {
      await this.router.navigate(['/meeting', 'feedback']);
    }
  }

  async endMeeting() {
    await this.room.send(JSON.stringify({type: 'endmeeting', id: this.localID}));
  }

  async stopStreams() {
    try {
      await this.localStream.mediaStream.getTracks().forEach(track => {
        track.stop();
      });
      if (this.localVideoStream.nativeElement.srcObject !== undefined) {
        await this.localVideoStream.nativeElement.srcObject.getTracks().forEach(track => {
          track.stop();
        });
      }
      await this.videoStreamDiv.forEach(el => {
        el.nativeElement.srcObject.getTracks().forEach(track => {
          track.stop();
        });
      });
      // todo: check if required?
      await this.localPublication.stop();
    } catch (e) {
      // console.log(e);
    }
  }

  async roomInit() {
    this.room = await new Owt.Conference.ConferenceClient(this.config.roomConfig);
    await this.room.addEventListener('streamadded', async streamEvent => {
      console.log('stream added', streamEvent);

      const stream = streamEvent.stream;

      if (stream.source.audio === 'mixed' && stream.source.video === 'mixed') {
        if (this.subscribeType !== this.SUBSCRIBETYPES.MIX) {
          this.modeName = 'Mix stream';
          return;
        }
      } else {
        if (stream.source.video === 'screen-cast') {
          await this.refreshForwardStreams();
          if (this.isLocalScreenSharing) {
            return;
          }
        } else if (this.subscribeType !== this.SUBSCRIBETYPES.FORWARD) {
          return;
        }
      }

      await stream.addEventListener('updated', (event) => {
        console.log('stream updated', event);
      });

      const streamSubscribed = await this.subscribeStream(stream);
      if (!streamSubscribed) {
        await this.subscribeStream(stream);
      }
    });
    await this.room.addEventListener('participantjoined', async (event) => {
      console.log('participantjoined...', event);

      await this.refreshAttendees();

      // await this.refreshForwardStreams();
      if (event.participant.userId !== 'user' /*&& getUserFromId(event.participant.id) === null*/) {
        const attendeeObject = this.attendeesMap.get(event.participant.userId);

        this.attendees[event.participant.id] = {
          id: event.participant.id,
          attendee_id: event.participant.userId,
          name: attendeeObject !== undefined ? attendeeObject.name : '',
          role: attendeeObject !== undefined ? attendeeObject.role : 'attendee',
          audio: false,
          video: false,
          speaking: false,
          loader: true
        };
        await this.attendeeList.push(event.participant.id);
        this.attendeeCount = await this.getAttendeeCount();
        event.participant.addEventListener('left', async () => {
          if (event.participant.id !== null && event.participant.userId !== undefined) {
            await this.removeAttendee(event.participant.id);
          } else {
          }
        });
        console.log('join user: ' + event.participant.userId);

        if (event.participant.id !== this.localID) {
          this.streamData[event.participant.id] = null;
        }

      }
    });

    await this.room.addEventListener('serverdisconnected', async (event) => {
      console.log('serverdisconnected', event);
      // await this.leaveMeeting(1);
      //// this.renderer.addClass(this.meetingEndedModal.nativeElement, 'show');
      // $('.MeetingEndedModal').show();
    });

    await this.room.addEventListener('messagereceived', async (event) => {
      console.log('messagereceived', event);
      const eventReceived = JSON.parse(event.message);
      if (eventReceived.type === 'mediaPermissionUpdated') {
        if (event.origin !== this.localID) {
          await this.updateAttendeeInfo();
        }
      }
      if (eventReceived.type === 'rolechanged') {
        Object.values(this.attendees).forEach((attendee) => {
          if (eventReceived.id !== undefined) {
            // @ts-ignore
            if (attendee.attendee_id === eventReceived.id) {
              // @ts-ignore
              attendee.role = eventReceived.role;
            }
          }
        });
        if (this.currentAttendee._id === eventReceived.id) {
          if (eventReceived.role !== undefined) {
            this.currentAttendee.role = eventReceived.role;
          }
          localStorage.setItem('currentAttendee', JSON.stringify(this.currentAttendee));
        }
      }
      if (eventReceived.type === 'participantremoved') {
        if (eventReceived.id !== undefined) {
          if (this.localID === eventReceived.id) {
            await this.leaveMeeting(1);
            this.kickOutMessage = 'A Host or Moderator has removed you from this meeting.';
            $('.MeetingEndedModal').show();
          }
        }
      }
      if (eventReceived.type === 'participantbanned') {
        if (eventReceived.id !== undefined) {
          if (this.localID === eventReceived.id) {
            let bannedFromMeetings = await JSON.parse(localStorage.getItem('bannedFromMeetings'));
            if (bannedFromMeetings === null || !Array.isArray(bannedFromMeetings)) {
              bannedFromMeetings = [this.currentMeeting.name];
            } else {
              bannedFromMeetings.push(this.currentMeeting.name);
            }
            await localStorage.setItem('bannedFromMeetings', JSON.stringify(bannedFromMeetings));

            await this.leaveMeeting(1);
            this.kickOutMessage = 'A Host or Moderator has banned you from this meeting.';
            $('.MeetingEndedModal').show();
          }
        }
      }
      if (eventReceived.type === 'participantmuted') {
        if (eventReceived.id !== undefined) {
          if (this.localID === eventReceived.id) {
            this.notify('You have been muted.');
          }
        }
      }
      if (eventReceived.type === 'participantrenamed') {
        if (eventReceived.new_name !== undefined && eventReceived.id !== undefined) {
          this.attendees[eventReceived.id].name = eventReceived.new_name;
        }
      }
      if (eventReceived.type === 'endmeeting') {
        if (eventReceived.id !== undefined) {
          if (this.localID !== eventReceived.id) {
            await this.leaveMeeting(1);
            $('.MeetingEndedModal').show();
          } else {
            await this.leaveMeeting();
          }
        }
      }
      if (eventReceived.type === 'stopscreenshare') {
        console.log(eventReceived.stream_id, this.localID, this.streamMap.get(eventReceived.stream_id).info.owner);
        // await this.refreshForwardStreams();
        if (this.streamMap.get(eventReceived.stream_id) !== undefined && this.streamMap.get(eventReceived.stream_id).info.owner === this.localID) {
          /*console.log('unpublishing share screen');
          await this.localScreen.mediaStream.getTracks().forEach(track => {
            track.stop();
          });
          await this.localScreenPublication.stop();
          //starting publication
          await this.createLocalStream();
          this.isLocalScreenSharing = false;
          this.isScreenSharing = false;*/
          await this.stopScreenShare();
        }
      }
      if (eventReceived.type === 'disablescreenshare') {
        // console.log('disablescreenshareevent', eventReceived);
        if (eventReceived.disable === 0) {
          if (this.currentAttendee.role === 'attendee') {
            this.displayScreenShare = false;
          }
        } else if (eventReceived.disable === 1) {
          if (this.currentAttendee.role === 'attendee' || this.currentAttendee.role === 'moderator') {
            this.displayScreenShare = false;
          }
        }
      }
      if (eventReceived.type === 'enablescreenshare') {
        if (eventReceived.enable === 0) {
          if (this.currentAttendee.role === 'attendee') {
            this.displayScreenShare = true;
          }
        } else if (eventReceived.enable === 1) {
          if (this.currentAttendee.role === 'attendee' || this.currentAttendee.role === 'moderator') {
            this.displayScreenShare = true;
          }
        }
      }
      if (eventReceived.type === 'screenWhenAlreadySharing') {
        this.screenWhenAlreadySharing = eventReceived.value;
        await localStorage.setItem('screenShare', JSON.stringify({
          screenShare: this.screenShareSettings.screenShare,
          screenWhenAlreadySharing: eventReceived.value
        }));
      }
      if (eventReceived.type === 'screenShare') {
        this.screenShare = eventReceived.value;
        await localStorage.setItem('screenShare', JSON.stringify({
          screenShare: eventReceived.value,
          screenWhenAlreadySharing: this.screenShareSettings.screenWhenAlreadySharing
        }));
      }
    });
  }

  async subscribeStream(stream) {
    console.log('********************subscribing stream*************');
    console.log(stream);
    console.log('***************************************************');

    let subscribeOptions = {};
    if (stream.settings.video.length === 0) {
      subscribeOptions = {
        audio: true,
        video: false
      };
    }
    if (stream.source.video === 'screen-cast') {
      subscribeOptions = {
        audio: false,
        video: true
      };
    }
    let success = false;
    await this.room.subscribe(stream, subscribeOptions).then(async (subscription) => {
      const streamDetails = await this.owtService.getStreamInfo(this.roomId, stream.id);
      console.log(streamDetails);
      if (streamDetails.media) {
        if (streamDetails.media.video) {
          if (streamDetails.media.video.status === 'active') {
            this.attendees[stream.origin].video = true;
          }
        }
      }
      if (streamDetails.info) {
        if (streamDetails.info.attributes) {
          if (streamDetails.info.attributes.audio === 'active') {
            this.attendees[stream.origin].audio = true;
          }
        }
      }

      // publication stopped...
      await subscription.addEventListener('error', async (event) => {
        console.log(event);
        console.log(stream);
        if (stream.source.video !== 'screen-cast') {
          if (this.attendees[stream.origin] !== undefined) {
            this.attendees[stream.origin].loader = true;
            this.attendees[stream.origin].video = false;
            this.attendees[stream.origin].audio = false;
          }
          if (stream.origin !== this.localID) {
            this.streamData[stream.origin] = null;
          }
          if (this.speakingIntervalSubscription[stream.origin]) {
            await this.speakingIntervalSubscription[stream.origin].unsubscribe();
            if (!!this.attendees[stream.origin]) {
              this.attendees[stream.origin].speaking = false;
            }
          }
        }
      });
      await subscription.addEventListener('mute', (event) => {
        if (event.kind === 'audio') {
          if (stream.origin === this.localID) {
            this.audioStatus = false;

            // this.notify('You have been muted.');
          } else {
            this.attendees[stream.origin].audio = false;
          }
        }
        if (event.kind === 'video') {
          if (stream.origin !== this.localID) {
            this.attendees[stream.origin].video = false;
          }
        }
        console.log('Stream Origin: ' + stream.origin + ' muted successfully');
      });
      await subscription.addEventListener('unmute', (event) => {
        if (event.kind === 'audio') {
          if (stream.origin === this.localID) {
            this.audioStatus = true;
            this.notify('You are unmuted.');
          } else {
            this.attendees[stream.origin].audio = true;
          }
        }
        if (event.kind === 'video') {
          if (stream.origin !== this.localID) {
            this.attendees[stream.origin].video = true;
          }
        }
        console.log('Stream Origin: ' + stream.origin + ' unmuted successfully');
      });
      if (this.localID !== stream.origin) {

        console.log('******************************************************');
        console.log('Attendees: ');
        console.log(this.attendees);
        console.log('Attendee present : ');
        console.log(this.attendees[stream.origin]);
        console.log('******************************************************');

        if (stream.origin !== this.localID) {
          console.log('----------------------------------------------------------------------------');
          console.log('stream', stream);
          console.log('----------------------------------------------------------------------------');
          if (stream.source.video !== 'screen-cast') {
            this.streamData[stream.origin] = stream.mediaStream;
            if (Object.keys(this.streamData).length === 1) {
              this.activeAttendeeID = Object.keys(this.streamData)[0];
            }
            if (!this.speakerStatus) {
              await this.videoStreamDiv.forEach(el => {
                el.nativeElement.volume = 0;
              });
            }
            if (this.attendees[stream.origin]) {
              this.attendees[stream.origin].loader = false;
            }
            if (this.sources.audioOut[0] && this.sources.audioOut[0] !== undefined) {
              if (this.selectedDevice.audioDestination !== this.sources.audioOut[0].deviceId) {
                await this.onDestinationChange();
              }
            }
          } else {
            this.isScreenSharing = true;
            await this.changeDetector.detectChanges();
            this.screenStream.nativeElement.srcObject = stream.mediaStream;
            this.screenSub = subscription;
            this.remoteScreenStream = stream;
            console.log(this.remoteScreenStream);
            stream.addEventListener('ended', async (event) => {
              this.isScreenSharing = false;
              this.remoteScreenStream = null;
            });
          }
        }

        if (stream.source.video === 'mixed') {
          this.remoteMixedSub = subscription;
        }

        this.speakingIntervalSubscription[stream.origin] = interval(250).subscribe(async () => {
          await subscription.getStats().then(report => {
            report.forEach((item) => {
              if (item.type === 'track' && item.kind === 'audio') {
                if (item.audioLevel > 0.08) {
                  if (!!this.attendees[stream.origin]) {

                    if (!this.attendeePinned) {
                      if (this.speakingStreak[stream.origin]) {
                        this.speakingStreak[stream.origin]++;
                        if (this.speakingStreak[stream.origin] >= 2) {
                          this.activeAttendeeID = stream.origin;
                        }
                      } else {
                        this.speakingStreak = {};
                        this.speakingStreak[stream.origin] = 1;
                      }
                    }


                    this.attendees[stream.origin].speaking = true;
                    if (!this.attendees[stream.origin].audio) {
                      this.attendees[stream.origin].audio = true;
                    }
                  }
                } else {
                  if (!!this.attendees[stream.origin]) {
                    this.attendees[stream.origin].speaking = false;
                  }
                }
                // console.log(item.audioLevel, this.speaker);
              }
              if (item.type === 'ssrc' && item.mediaType === 'video') {
                this.scaleLevel = parseInt(item.googFrameHeightReceived) / parseInt(item.googFrameWidthReceived);
              }
            });
            // resizeStream(mode);
          }, err => {
            console.error('stats error: ' + err);
          });
        });
      }
      success = true;
    }, err => {
      console.error('subscribe error: ' + err);
      this.attendees[stream.origin].loader = false;
      this.attendees[stream.origin].video = false;
      this.attendees[stream.origin].audio = false;
    });
    console.log('Stream success: ' + success);
    return success;
  }

  async joinMeeting() {
    await this.room.join(this.token).then(async res => {
      // todo: add meeting attendee in dashboard component
      const permissions = {
        audio: this.mediaPermissions.audio,
        video: this.mediaPermissions.video
      };
      await this.owtService.updateParticipantPermissions(this.roomId, res.self.id, permissions);
      await this.room.send(JSON.stringify({type: 'mediaPermissionUpdated'}));

      this.joinedAttendees = res.participants;
      const streams = res.remoteStreams;
      console.log('streams', streams);
      await this.refreshForwardStreams();
      await this.refreshAttendees();
      await this.joinedAttendees.map(async (participant) => {
        await participant.addEventListener('left', async () => {
          await this.removeAttendee(participant.id);
        });
        console.log('joined meeting: ', participant);
        this.localID = participant.id;
        await localStorage.setItem('localID', JSON.stringify(this.localID));

        const attendeeObject = await this.attendeesMap.get(participant.userId);

        const attendee = {
          id: participant.id,
          attendee_id: participant.userId,
          name: attendeeObject !== undefined ? attendeeObject.name : '',
          role: attendeeObject !== undefined ? attendeeObject.role : 'attendee',
          audio: false,
          video: false,
          speaking: false,
          loader: true
        };
        this.streamMap.forEach(async (stream) => {
          if (stream.info.owner === participant.id && stream.media.audio) {
            if (stream.media.audio.status === 'active') {
              attendee.audio = true;
            }
          }
          if (stream.info.owner === participant.id && stream.media.video) {
            if (stream.media.video.status === 'active') {
              attendee.video = true;
            }
          }
        });
        this.attendees[participant.id] = attendee;
        if (participant.id !== this.localID) {
          await this.attendeeList.push(participant.id);
        }
        this.attendeeCount = await this.getAttendeeCount();
      });


      /*const permissions = {
        audio: this.mediaPermissions.audio,
        video: this.mediaPermissions.video
      };
      await this.owtService.updateParticipantPermissions(this.roomId, this.localID, permissions);*/

      if (this.mediaPermissions.audio || this.mediaPermissions.video) {
        await this.createLocalStream(this.videoStatus);
      }

      this.localStreamLoaded = true;
      const failedSubscriptions = [];
      for (const stream of streams) {
        if (stream.source.audio === 'mixed' && stream.source.video === 'mixed') {
          console.log('Mix stream id: ' + stream.id);
          stream.addEventListener('layoutChanged', function (regions) {
            console.log('stream', stream.id, 'VideoLayoutChanged');
            // currentRegions = regions;
          });
        }
        console.log('stream in conference:', stream.id);
        const isMixStream = (stream.source.audio === 'mixed');
        if ((this.subscribeType === this.SUBSCRIBETYPES.FORWARD && !isMixStream) ||
          (this.subscribeType === this.SUBSCRIBETYPES.MIX && isMixStream) ||
          (stream.source.video === 'screen-cast')) {
          const streamSubscribed = await this.subscribeStream(stream);
          if (!streamSubscribed) {
            failedSubscriptions.push(stream);
          }
        }
      }
      for (const failedStream of failedSubscriptions) {
        await this.subscribeStream(failedStream);
      }


      for (const attendee of this.joinedAttendees) {
        if (attendee.id === this.joinedAttendees[this.joinedAttendees.length - 1].id) {
          continue;
        }
        if (!this.streamData[attendee.id]) {
          this.streamData[attendee.id] = null;
        }
      }
      await this.updateAttendeeInfo();
      await this.refreshForwardStreams();
    });

  }

  async createLocalStream(videoStatus = true) {
    let mediaStream;
    console.log('CONSTRAINTS:::');
    console.log(this.avTrackConstraint);
    let audioConstraints = new Owt.Base.AudioTrackConstraints(Owt.Base.AudioSourceInfo.MIC);
    audioConstraints.deviceId = this.avTrackConstraint.audio.deviceId;
    let videoConstraints;
    if (videoStatus && this.mediaPermissions.video) {
      videoConstraints = new Owt.Base.VideoTrackConstraints(Owt.Base.VideoSourceInfo.CAMERA);
      videoConstraints.deviceId = this.avTrackConstraint.video.deviceId;
      videoConstraints.frameRate = this.avTrackConstraint.video.frameRate;
      videoConstraints.resolution = this.avTrackConstraint.video.resolution;
    } else {
      videoConstraints = false;
      this.videoStatus = false;
      // @ts-ignore
      this.avTrackConstraint.video = false;
    }

    if (!this.mediaPermissions.audio) {
      audioConstraints = false;
      this.audioStatus = false;
      // @ts-ignore
      this.avTrackConstraint.audio = false;
    }

    const streamConstraints = new Owt.Base.StreamConstraints(audioConstraints, videoConstraints);
    console.log(this.avTrackConstraint);
    console.log(streamConstraints);
    console.log(this.avTrackConstraint);
    await Owt.Base.MediaStreamFactory.createMediaStream(this.avTrackConstraint).then(async stream => {
      mediaStream = stream;
      console.log('Success to create MediaStream');
      const streamAttributes = {
        audio: this.audioStatus ? 'active' : 'inactive'
      };

      const audioSource = this.mediaPermissions.audio ? 'mic' : undefined;
      const videoSource = this.mediaPermissions.video ? 'camera' : undefined;

      console.log(audioSource, videoSource);
      console.log(streamConstraints);

      this.localStream = await new Owt.Base.LocalStream(
        mediaStream, new Owt.Base.StreamSourceInfo(
          audioSource, videoSource), streamAttributes
      );
      console.log(this.localStream);
      this.localVideoStream.nativeElement.volume = 0;
      this.localVideoStream.nativeElement.srcObject = this.localStream.mediaStream;

      let publishOptions;
      if (videoStatus && this.mediaPermissions.video) {
        publishOptions = {
          audio: this.mediaPermissions.audio,
          // @ts-ignore
          video: [
            {rid: 'f', active: true, scaleResolutionDownBy: 1.0},
            {rid: 'h', active: true, scaleResolutionDownBy: 2.0},
            {rid: 'q', active: true, scaleResolutionDownBy: 4.0}
          ]
        };
      }

      this.publishRoom(publishOptions).then(async publication => {
        this.localPublication = publication;
        if (!this.audioStatus) {
          await this.disableMedia('audio');
        }
        console.log('publish success');
        if (!!this.localStream) {
        }

        // Todo: You are speaking...
        let flag = 0;
        this.speakingSubscription = interval(250).subscribe(async () => {
          this.speaking = false;
          await publication.getStats().then(report => {
            report.forEach((item) => {
              if (item.kind === 'audio' && item.type === 'media-source') {
                // console.log(item);
                if (item.audioLevel > 0.08) {
                  if (!this.audioStatus) {
                    if (flag === 0) {
                      this.notify('You are muted.');
                      flag = 1;
                    } else {
                      flag = 0;
                    }
                  } else {
                    this.speaking = true;
                  }
                } else {
                  if (this.audioStatus) {
                    this.speaking = false;
                  } else {
                  }
                }
                /*if (item.audioLevel > 0.08) {
                  this.speaker = 'You are ';
                } else {
                  this.speaker = null;
                }*/
                // console.log(item.audioLevel, this.speaker);
              }
            });
          }, err => {
            console.error('stats error: ' + err);
          });

        });

        await publication.addEventListener('error', (err) => {
          console.log('Publication error: ' + err.error.message);
        });
      }, err => {
        console.log('Publish error: ' + err);
      });
    }, async (err) => {
      this.audioStatus = false;
      this.videoStatus = false;
      this.localMediaCreationError = true;
      console.error('Failed to create MediaStream, ' + err);
      if (err.name === 'OverconstrainedError') {
        /*if (confirm('your camera can\'t support the resolution constraints, please leave room and select a lower resolution')) {
          // userExit();
        }*/
      }
    });
  }

  async shareScreen() {
    if (this.isLocalScreenSharing === false) {

      if (this.remoteScreenStream !== null) {
        // await this.owtService.dropStream(this.roomId, this.remoteScreenStream.id);
        await this.room.send(JSON.stringify({type: 'stopscreenshare', stream_id: this.remoteScreenStream.id}));
      }

      this.isLocalScreenSharing = true;
      // this.isScreenSharing = true;
      await this.changeDetector.detectChanges();
      console.log(screen.width, screen.height, screen.availHeight);
      const screenSharingConfig = {
        audio: {
          source: 'screen-cast',
        },
        video: {
          resolution: {
            width: screen.width,
            height: screen.availHeight// screen.height - 80
          },
          frameRate: 20,
          source: 'screen-cast'
        },
        // extensionId: 'pndohhifhheefbpeljcmnhnkphepimhe'//chrome.runtime.id
      };

      await Owt.Base.MediaStreamFactory.createMediaStream(screenSharingConfig).then(async (stream) => {
        if (this.screenWhenAlreadySharing === 1) {
          await this.room.send(JSON.stringify({type: 'disablescreenshare', disable: 1}));
        }
        // await navigator.mediaDevices.getUserMedia(constraints).then(async (stream) => {
        this.localScreen = new Owt.Base.LocalStream(stream, new Owt.Base.StreamSourceInfo('screen-cast', 'screen-cast'));
        console.info(this.localScreen);
        this.localScreenId = this.localScreen.id;
        const screenVideoTracks = this.localScreen.mediaStream.getVideoTracks();
        console.log(this.isScreenSharing);
        // this.screenStream.nativeElement.srcObject = this.localScreen.mediaStream;
        for (const screenVideoTrack of screenVideoTracks) {
          screenVideoTrack.addEventListener('ended', async (e) => {
            /*console.log('unpublish');
            await this.localScreenPublication.stop();
            //starting publication
            await this.createLocalStream();
            this.isLocalScreenSharing = false;
            this.isScreenSharing = false;*/
            await this.stopScreenShare(1);
            if (this.screenWhenAlreadySharing === 1) {
              await this.room.send(JSON.stringify({type: 'enablescreenshare', enable: 1}));
            }
          });
        }
        this.room.publish(this.localScreen).then(publication => {
          console.info('publish success');
          this.localScreenPublication = publication;
        }, err => {
          console.error('local-screen publish failed');
        });
      }, (err) => {
        console.error('create local-screen failed');
        this.isLocalScreenSharing = false;
        this.isScreenSharing = false;
      });
    }
  }

  async stopScreenShare(stopTracks = 0) {
    console.log('unpublishing share screen');
    if (stopTracks === 0) {
      await this.localScreen.mediaStream.getTracks().forEach(track => {
        track.stop();
      });
    }
    await this.localScreenPublication.stop();
    // starting publication
    this.isLocalScreenSharing = false;
    this.isScreenSharing = false;
  }


  async publishRoom(publishOptions) {
    if (!!publishOptions) {
      return await this.room.publish(this.localStream, publishOptions);
    } else {
      return await this.room.publish(this.localStream);
    }
  }


  async removeAttendee(id) {
    if (id === undefined) {
      return;
    }
    delete this.attendees[id];
    const deleteIndex = this.attendeeList.indexOf(id);
    if (deleteIndex > -1) {
      this.attendeeList.splice(deleteIndex, 1);
    }
    this.attendeeCount = await this.getAttendeeCount();
    delete this.streamData[id];
  }

  async removeParticipant(id) {
    await this.room.send(JSON.stringify({type: 'participantremoved', id}));
    await this.removeAttendee(id);
  }

  async enableRenameMode(id) {
    this.activeRenameID = id;
  }

  async disableRenameMode() {
    this.activeRenameID = null;
  }

  async renameParticipant(newName) {
    if (!this.activeRenameID) {
      return;
    }
    await this.meetingService.updateMeetingAttendee(
      this.currentMeeting._id,
      this.attendees[this.activeRenameID].attendee_id,
      {name: newName});
    await this.room.send(JSON.stringify({type: 'participantrenamed', id: this.activeRenameID, new_name: newName}));
    this.activeRenameID = null;
  }

  async banParticipant(id) {
    await this.room.send(JSON.stringify({type: 'participantbanned', id}));
    await this.removeAttendee(id);
  }

  async makeModerator(attendeeID) {
    await this.meetingService.updateMeetingAttendee(this.currentMeeting._id, attendeeID, {role: 'moderator'});
    await this.room.send(JSON.stringify({type: 'rolechanged', id: attendeeID, role: 'moderator'}));
  }

  async onChangeAudioStatus(id) {
    await this.refreshForwardStreams();
    this.streamMap.forEach(async (stream, i) => {
      if (stream.info.owner === id && stream.media.audio) {
        if (stream.media.audio.status === 'active') {
          console.log('muted ' + stream.id + ' successfully');
          await this.owtService.pauseStream(this.roomId, stream.id, 'audio');
          await this.room.send(JSON.stringify({type: 'participantmuted', id}));
        }
      }
    });
  }

  async onMuteAll() {
    await this.refreshForwardStreams();
    await this.streamMap.forEach(async (stream, i) => {
      if (stream.info.owner !== this.localID && stream.media.audio) {
        if (stream.media.audio.status === 'active') {
          console.log('muted ' + stream.id + ' successfully');
          await this.owtService.pauseStream(this.roomId, stream.id, 'audio');
          await this.room.send(JSON.stringify({type: 'participantmuted', id: stream.info.owner}));
        }
      }
    });
  }

  async refreshForwardStreams() {
    this.streamMap = new Map();
    const streams = await this.owtService.getStreams(this.roomId);
    console.log(streams);
    this.streamMap.clear();
    for (const stream of streams) {
      if (stream.type === 'forward') {
        this.streamMap.set(stream.id, stream);
      }
    }
    console.log(this.streamMap);
  }

  async refreshAttendees() {
    this.attendeesMap.clear();
    const attendees = await this.meetingService.getMeetingAttendees(this.currentMeeting._id);
    for (const attendee of attendees) {
      this.attendeesMap.set(attendee._id, attendee);
    }
    console.log(this.attendeesMap);
  }

  async getAttendeeCount() {
    this.attendeesCount = Object.keys(this.attendees).length;
    /*if (attendeeCount < 10) {
      return '0' + attendeeCount;
    }*/
    return this.attendeesCount.toString();
  }

  async ngOnDestroy() {
    await this.stopCameraCapture();
    if (this.timerSubscription) {
      await this.timerSubscription.unsubscribe();
    }
    if (this.speakingSubscription) {
      await this.speakingSubscription.unsubscribe();
    }
    /*if (this.room) {
      await this.room.clearEventListener('serverdisconnected');
    }*/

    await localStorage.removeItem('meetingInProgress');
    // todo: user doesn't leave room when pressing back button and a new stream gets added
    // await this.room.leave();
  }

  openAttendees(event, switchTabs = true) {
    event.preventDefault();

    if (this.sideBarStatus) {
      if (switchTabs) {
        if (this.participantTab.nativeElement.classList.contains('active')) {
          this.sideBarStatus = false;
        }
      } else {
        this.sideBarStatus = false;
      }

    } else {
      this.sideBarStatus = true;
      // this.renderer.addClass(this.menuToggle.nativeElement, 'close-icon');
    }

    if (!this.participantTab.nativeElement.classList.contains('active')) {
      // $('.nav-tabs a[href="#TM-Tab2"]').removeClass('active');
      // $('#TM-Tab2').removeClass('active');
      this.renderer.removeClass(this.inviteLink.nativeElement, 'active');
      this.renderer.removeClass(this.inviteTab.nativeElement, 'active');
      this.renderer.addClass(this.participantLink.nativeElement, 'active');
      this.renderer.addClass(this.participantTab.nativeElement, 'active');
    }
    /*if (this.menuToggle.nativeElement.classList.contains('close-icon')) {
      this.menuToggle.nativeElement.children[0].innerText = 'close';
    } else {
      this.menuToggle.nativeElement.children[0].innerText = 'menu';
    }*/
  }


  openInvite(event) {
    event.preventDefault();
    this.sideBarStatus = !this.sideBarStatus;

    if (!this.inviteTab.nativeElement.classList.contains('active')) {
      this.renderer.removeClass(this.participantLink.nativeElement, 'active');
      this.renderer.removeClass(this.participantTab.nativeElement, 'active');
      this.renderer.addClass(this.inviteLink.nativeElement, 'active');
      this.renderer.addClass(this.inviteTab.nativeElement, 'active');
    }
  }


  getDefaultMediaStatus(kind) {
    try {
      if (kind !== 'audio' || kind !== 'video') {
        return false;
      }
      return this.mediaConstraints[kind].status;
    } catch (e) {
      return false;
    }
  }

  async toggleAudio() {
    if (!this.localMediaCreationError && this.mediaPermissions.audio) {
      if (this.audioStatus) {
        await this.disableMedia('audio');
      } else {
        await this.enableMedia('audio');
      }
    }
  }


  async toggleVideo() {
    if (!this.localMediaCreationError && this.mediaPermissions.video) {
      if (this.videoStatus) {
        await this.disableMedia('video');
      } else {
        await this.enableMedia('video');
      }
    }
  }

  async enableMedia(kind) {
    if (!this.localPublication) {
      return;
    }

    if (kind === 'audio') {
      this.localPublication.unmute(kind).then(
        () => {
          console.log('unmute successfully');
          this.audioStatus = true;
        }, err => {
          console.log('unmute failed');
        });
    }
    if (kind === 'video') {
      console.log('stopping publication');
      if (this.localStream !== null) {
        await this.localStream.mediaStream.getTracks().forEach(track => {
          track.stop();
        });
      }
      this.localStream = null;
      await this.localPublication.stop();
      this.avTrackConstraint.video = {
        source: 'camera',
        resolution: this.selectedResolution,
        frameRate: 24,
        // @ts-ignore
        deviceId: this.selectedDevice.video
      };

      console.log('starting publication');
      await this.createLocalStream(true);
      this.videoStatus = true;
    }

  }

  async disableMedia(kind) {
    if (!this.localPublication) {
      return;
    }

    if (kind === 'audio') {
      this.localPublication.mute(kind).then(
        () => {
          this.notify('You are muted.');
          console.log('mute successfully');
          this.audioStatus = false;
        }, err => {
          console.log('mute failed');
        });
    }
    if (kind === 'video') {
      console.log('stopping publication');
      if (this.localStream !== null) {
        await this.localStream.mediaStream.getTracks().forEach(track => {
          track.stop();
        });
      }
      this.localStream = null;
      this.localVideoStream.nativeElement.srcObject = null;
      await this.localPublication.stop();
      // @ts-ignore
      this.avTrackConstraint.video = false;
      console.log('starting publication');
      await this.createLocalStream(false);
      this.videoStatus = false;
    }
  }

  async onDeviceChange(kind, event) {
    event.stopPropagation();
    try {
      let deviceID;
      if (event.type === 'click') {
        deviceID = event.target.id;
      } else if (event.type === 'change') {
        deviceID = this.sources.audioIn[event.target.selectedIndex].deviceId;
      }
      this.selectedDevice[kind] = deviceID;
      if (this.localStream === null) {
        return;
      }
      console.log('stopping publication');
      await this.localStream.mediaStream.getTracks().forEach(track => {
        track.stop();
      });
      await this.localPublication.stop();
      this.localStream = null;

      if (kind === 'audio') {
        this.avTrackConstraint.audio = {
          source: 'mic',
          // @ts-ignore
          deviceId: this.selectedDevice.audio
        };
        console.log('starting publication');
        await this.createLocalStream(this.videoStatus);
        console.log('audio device switched');
      }
      if (kind === 'video') {
        this.avTrackConstraint.video = {
          source: 'camera',
          resolution: this.selectedResolution,
          frameRate: 24,
          // @ts-ignore
          deviceId: this.selectedDevice.video
        };
        console.log('starting publication');
        await this.createLocalStream(this.videoStatus);
        console.log('video device switched');
      }
    } catch (e) {
      console.log(e);
    }
  }

  async onSpeakerDeviceChange(event) {
    event.stopPropagation();
    try {
      let deviceID;
      if (event.type === 'click') {
        deviceID = event.target.id;
      } else if (event.type === 'change') {
        deviceID = this.sources.audioOut[event.target.selectedIndex].deviceId;
      }
      this.selectedDevice.audioDestination = deviceID;
      await this.onDestinationChange();
    } catch (e) {
      console.log(e);
    }
  }

  async muteIfSpeakerOff() {
    if (!this.speakerStatus) {
      await this.videoStreamDiv.forEach(el => {
        el.nativeElement.volume = 0;
      });
    }
  }

  async onSpeakerTest() {
    this.audio = new Audio();
    this.audio.src = '../../../assets/ringtones/ringtone.mp3';
    await this.audio.load();
    await this.audio.play();
    /*this.instantVolume = this.audio.volume;
    this.instantVolume *= 100;
    console.log(this.instantVolume);*/
  }

  async startCameraCapture() {
    const constraints = {
      audio: false,
      video: {deviceId: {exact: this.selectedDevice.video}}
    };
    console.log(constraints);
    await navigator.mediaDevices.getUserMedia(constraints).then(async (stream) => {
      await this.stopStreaming();
      this.cameraMediaStream = stream;
      console.log(this.cameraMediaStream);
    });
  }

  async stopCameraCapture() {
    if (this.cameraMediaStream !== undefined) {
      await this.cameraMediaStream.getTracks().forEach(track => {
        track.stop();
      });
    }
  }

  async onCameraTest() {
    await this.startCameraCapture();
  }

  async onVideoSettings() {
    // console.log(this.cameraMediaStream);
    await this.startCameraCapture();
    // this.videoSettingsPreview.nativeElement.srcObject = this.cameraMediaStream;
  }

  async onMicrophoneTest() {
    await this.audio.pause();
    const constraints = {
      audio: {deviceId: {exact: this.avTrackConstraint.audio.deviceId}, echoCancellation: true},
      video: false
    };
    console.log(constraints);
    const audio = new Audio();
    await navigator.mediaDevices.getUserMedia(constraints).then(async (stream) => {
      await this.stopStreaming();
      console.log(stream);
      audio.srcObject = stream;
      audio.volume = 0.5;
      await audio.play();
      this.mediaStream = stream;

      const audioContext = new AudioContext();
      this.microphone = audioContext.createMediaStreamSource(stream);
      this.javascriptNode = audioContext.createScriptProcessor(1024, 1, 1);
      this.microphone.connect(this.javascriptNode);
      this.javascriptNode.connect(audioContext.destination);
      this.javascriptNode.onaudioprocess = async (event) => {
        const input = await event.inputBuffer.getChannelData(0);
        let i;
        let sum = 0.0;
        for (i = 0; i < input.length; ++i) {
          sum += input[i] * input[i];
        }
        this.instant = await Math.sqrt(sum / input.length).toFixed(2);
        this.instant *= 100;
      };
    });
  }

  async onMicrophoneTestSuccess() {
    await this.stopStreaming();
    await this.microphone.disconnect();
    await this.javascriptNode.disconnect();
  }

  async stopStreaming() {
    if (this.mediaStream !== undefined) {
      await this.mediaStream.getTracks().forEach(track => {
        track.stop();
      });
    }
  }

  async onCloseModal() {
    await this.audio.pause();
    await this.stopStreaming();
  }

  addtestvideo() {
    console.log(this.activeAttendeeID);
    this.avTrackConstraint.video = {
      source: 'camera',
      resolution: new Owt.Base.Resolution(640, 360),
      frameRate: 24,
      // @ts-ignore
      deviceId: this.selectedDevice.video
    };
    Owt.Base.MediaStreamFactory.createMediaStream(this.avTrackConstraint).then(async stream => {
      const ls = await new Owt.Base.LocalStream(
        stream, new Owt.Base.StreamSourceInfo(
          'mic', 'camera')
      );
      const x = Math.random().toString(36).substring(7);
      this.streamData[x] = ls.mediaStream;
      this.attendees[x] = {
        id: x,
        name: 'test',
        role: 'a',
        audio: true,
        video: true,
        speaking: false,
        loader: false
      };
      if (Object.keys(this.streamData).length === 1) {
        if (!this.activeAttendeeID) {
          this.activeAttendeeID = Object.keys(this.streamData)[0];
        }
      }
      // @ts-ignore
      this.attendeeCount = Object.keys(this.streamData).length + 1;
      this.attendeeList.push(x);
    });
  }

  async onSettingsModalClick(event) {
    if (event.target.classList.contains('SettingModal') || event.target.classList.contains('close') || event.target.classList.contains('material-icons')) {
      await this.stopVideoTestStream();
    }
  }

  async onSpeakerTestClick(event) {
    if (event.target.classList.contains('SpeakerModal') || event.target.classList.contains('close') || event.target.classList.contains('material-icons')) {
      await this.audio.pause();
    }
  }

  async onMicrophoneTestClick(event) {
    if (event.target.classList.contains('MicrophoneTestModal1') || event.target.classList.contains('close') || event.target.classList.contains('material-icons')) {
      await this.stopAudioTestStream();
    }
  }

  async onVideoTestClick(event) {
    if (event.target.classList.contains('VideoCameraModal1') || event.target.classList.contains('close') || event.target.classList.contains('material-icons')) {
      await this.stopVideoTestStream();
    }
  }

  async onMeetingEndedClick(event) {
    if (event.target.classList.contains('MeetingEndedModal') || event.target.classList.contains('close') || event.target.classList.contains('material-icons')) {
      await this.redirectToFeedback();
    }
  }

  async stopAudioTestStream() {
    if (this.mediaStream !== undefined) {
      await this.mediaStream.getTracks().forEach(track => {
        track.stop();
      });
    }
  }

  async stopVideoTestStream() {
    if (this.cameraMediaStream !== undefined) {
      await this.cameraMediaStream.getTracks().forEach(track => {
        track.stop();
      });
    }
  }

  async attachDeviceId(element, deviceId) {
    if (typeof element.sinkId !== 'undefined') {
      if (element.sinkId !== deviceId) {
        await element.setSinkId(deviceId)
          .catch(error => {
            let errorMessage = error;
            if (error.name === 'SecurityError') {
              errorMessage = `You need to use HTTPS for selecting audio output device: ${error}`;
            }
            alert(errorMessage);
            if (this.sources.audioOut[0] && this.sources.audioOut[0] !== undefined) {
              this.selectedDevice.audioDestination = this.sources.audioOut[0].deviceId;
            }
          });
      }
    } else {
      alert('Browser does not support output device selection.');
    }
  }

  async onDestinationChange() {
    await this.videoStreamDiv.forEach((element) => {
      this.attachDeviceId(element.nativeElement, this.selectedDevice.audioDestination);
    });
  }

  async redirectToFeedback() {
    await this.router.navigate(['/meeting', 'feedback']);
  }


  onLayoutSelect(layout) {
    if (layout === 'SIDEBAR' || layout === 'SPEAKER') {
      this.toasterService.info('The selected layout is not available.');
      return;
    }
    this.selectedLayoutOption = layout;
  }

  getCarouselPages() {
    const carouselAttendeeList = Array.from(this.attendeeList);
    carouselAttendeeList.splice(carouselAttendeeList.indexOf(this.activeAttendeeID), 1);
    const pageCount = Math.ceil((carouselAttendeeList.length) / 6);
    return Array(pageCount).fill(0).map((x, i) => i);
  }

  getSplicedAttendees(pageNumber) {
    const carouselAttendeeList = Array.from(this.attendeeList);
    carouselAttendeeList.splice(carouselAttendeeList.indexOf(this.activeAttendeeID), 1);
    return carouselAttendeeList.slice(pageNumber * 6, (pageNumber * 6) + 6);
  }

  onCopy() {
    this.renderer.addClass(this.copyButton.nativeElement, 'copied');
    /*this.toasterService.info('copied', '', {
      disableTimeOut: true
    });*/
    setTimeout(() => {
      this.renderer.removeClass(this.copyButton.nativeElement, 'copied');
    }, 600);
  }

  async onChangeScreenShareSettings(type, value) {
    // console.log(type + ' change screen share settings ' + value);
    if (type === 0) {
      this.screenShare = value;
      this.room.send(JSON.stringify({type: 'screenShare', value}));
    } else if (type === 1) {
      this.screenWhenAlreadySharing = value;
      this.room.send(JSON.stringify({type: 'screenWhenAlreadySharing', value}));
    }

    if (this.screenShare === 0) {
      await this.room.send(JSON.stringify({type: 'enablescreenshare', enable: 0}));
    } else if (this.screenShare === 1) {
      await this.room.send(JSON.stringify({type: 'disablescreenshare', disable: 0}));
    }

    const screenShareSettings = {
      screenShare: this.screenShare,
      screenWhenAlreadySharing: this.screenWhenAlreadySharing
    };
    await localStorage.setItem('screenShare', JSON.stringify(screenShareSettings));
    // console.log(this.screenShare, this.screenWhenAlreadySharing);
  }

  async updateAttendeeInfo() {
    const participants = await this.owtService.getParticipants(this.roomId);
    console.log(participants);
    participants.forEach(participant => {
      if (!!this.attendees[participant.id] && !!participant.permission) {
        if (!participant.permission.publish.audio) {
          this.attendees[participant.id].audio = false;
        }
        if (!participant.permission.publish.video) {
          this.attendees[participant.id].video = false;
        }
        if (!participant.permission.publish.audio && !participant.permission.publish.video) {
          this.attendees[participant.id].loader = false;
        }
      }

    });
  }

  pinAttendee(attendeeID) {
    this.attendeePinned = true;
    this.activeAttendeeID = attendeeID;
  }

  unpinAttendee() {
    this.attendeePinned = false;
  }

  notify(message) {
    this.renderer.setAttribute(this.popUp.nativeElement, 'data-content', message);
    this.renderer.addClass(this.popUp.nativeElement, 'notify');
    setTimeout(() => {
      this.renderer.removeClass(this.popUp.nativeElement, 'notify');
    }, 1500);
  }

  async test() {
    // this.addtestvideo();


    // console.log(participantDetails);
    // console.log(this.streamData);
    // console.log(this.attendees);
    // console.log(this.localID);
  }

}
