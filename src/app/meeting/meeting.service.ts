import {Injectable} from '@angular/core';
import {RequestService} from '../services/request.service';
import {Config} from '../services/config';
import {WebsocketService} from './web-socket.service';
import {Subject} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class MeetingService {

  url = '/api/owt/';
  baseUrl: string = this.config.rootUrl + '/api/meetings';
  //meetingData: any;
  attendeeData = {
    name: 'Anonymous',
    role: null
  };

  /*participantSocket: Subject<any>;
  attendeeSocket: Subject<any>;
  streamSocket: Subject<any>;
  attendeeUpdateSocket: Subject<any>;*/

  constructor(private requestService: RequestService, public config: Config, private webSocketService: WebsocketService) {
    this.url = config.rootUrl + this.url;

    /*this.participantSocket = <Subject<any>>webSocketService
      .connect('participant').pipe(tap((response) => {
        console.log(response);
        return response;
      }));

    this.attendeeSocket = <Subject<any>>webSocketService
      .connect('attendee').pipe(tap((response) => {
        console.log(response);
        return response;
      }));

    this.streamSocket = <Subject<any>>webSocketService
      .connect('stream').pipe(tap((response) => {
        console.log(response);
        return response;
      }));

    this.attendeeUpdateSocket = <Subject<any>>webSocketService
      .connect('attendeeupdated').pipe(tap((response) => {
        console.log(response);
        return response;
      }));*/
  }

  createToken(roomID, body) {
    return this.requestService.observableRequest('post', `${this.url}${roomID}/tokens`, body);
  }

  async index(options?: { name?: string, meeting_code?: string }): Promise<any> {
    let url = this.baseUrl;
    if (options !== null) {
      if (options.name) {
        url += '?name=' + options.name;
      }
      /*if (options.meeting_code) {
        url += '&meeting_code=' + options.meeting_code;
      }*/
    }
    return await this.requestService.request('get', url);
  }

  /*async getMeeting(meeting_name, meeting_code): Promise<any> {
    return await this.requestService.request('get', this.baseUrl + '?name=' + meeting_name + '&meeting_code=' + meeting_code);
  }*/

  async getMeeting(meeting_id): Promise<any> {
    return await this.requestService.request('get', this.baseUrl + '/' + meeting_id);
  }

  async createMeeting(meeting): Promise<any> {
    return await this.requestService.request('post', this.baseUrl, meeting);
  }

  async leaveMeeting(meeting_id, attendee_id): Promise<any> {
    return await this.requestService.request('delete', this.baseUrl + '/' + meeting_id + '/meeting-attendees/' + attendee_id);
  }

  async endMeeting(meeting_id): Promise<any> {
    return await this.requestService.request('delete', this.baseUrl + '/' + meeting_id);
  }

  async validateMeeting(meeting) {
    return await this.requestService.request('post', this.baseUrl + '/validate', meeting);
  }

  async addMeetingAttendee(meeting_id, attendee) {
    return await this.requestService.request('post', this.baseUrl + '/' + meeting_id + '/meeting-attendees', attendee);
  }

  async getMeetingAttendees(meeting_id) {
    return await this.requestService.request('get', this.baseUrl + '/' + meeting_id + '/meeting-attendees');
  }

  async updateMeetingAttendee(meeting_id, attendee_id, data) {
    return await this.requestService.request('put', this.baseUrl + '/' + meeting_id + '/meeting-attendees/' + attendee_id, data);
  }

  async updateAttendeeFeedback(meeting_id, attendee_id, data) {
    return await this.requestService.request('post', this.baseUrl + '/' + meeting_id + '/meeting-attendees/' + attendee_id + '/feedback', data);
  }

  async exitMeeting(data) {
    return await this.requestService.request('post', this.baseUrl + '/exit-meeting', data);
  }

  async startMeeting(meetingID) {
    return await this.requestService.request('put', this.baseUrl + '/' + meetingID + '/start');
  }

  async verifyCaptcha(data) {
    return await this.requestService.request('post', this.baseUrl + '/verify', data);
  }

}
