import {Component, ElementRef, HostListener, NgZone, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {
  faMicrophone,
  faMicrophoneSlash,
  faVideo,
  faVideoSlash,
} from '@fortawesome/free-solid-svg-icons';
import {MeetingService} from '../meeting.service';
import {HelperService} from '../../services/helper.service';
import {OwtService} from '../owt.service';
import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'app-waiting-room',
  templateUrl: './waiting-room.component.html',
  styleUrls: ['../../../assets/css/style.css', './waiting-room.component.css'],
})
export class WaitingRoomComponent implements OnInit, OnDestroy {

  constructor(private renderer: Renderer2,
              private router: Router,
              private meetingService: MeetingService,
              private owtService: OwtService,
              public helperService: HelperService,
              public ngZone: NgZone,
              public toasterService: ToastrService,
              private elementRef: ElementRef) {
  }

  @ViewChild('audioSource', {static: false}) audioSource: ElementRef;
  @ViewChild('videoSource', {static: false}) videoSource: ElementRef;
  @ViewChild('audioDestination', {static: false}) audioDestination: ElementRef;
  @ViewChild('videoPreview', {static: false}) videoPreview: ElementRef;
  @ViewChild('videoSettingPreview', {static: false}) videoSettingPreview: ElementRef;

  @ViewChild('copyButton', {static: false}) copyButton: ElementRef;

  public mediaStream;
  public settingsMediaStream;
  public sources: any = {
    audioIn: [],
    audioOut: [],
    video: []
  };
  public mediaStatus: any = {
    audio: true,
    video: true
  };
  public icons = {
    mute: faMicrophone,
    unmute: faMicrophoneSlash,
    video_on: faVideo,
    video_off: faVideoSlash
  };

  currentAttendee: any;
  currentMeeting: any;
  meetingDetails: any;
  cameraError: string = null;
  micError: string = null;
  participantNames: string = '';
  meetingType: string = 'Start meeting';
  instant: any;
  microphone: any;
  javascriptNode: any;
  private socketSubscription: any;
  public cameraVideoLoading = true;
  public settingsCameraVideoLoading = false;

  public isSettingsModalOpen = false;

  private mediaPermissions = {
    audio: false,
    video: false
  };

  resolutions = [
    {
      name: 'QQVGA',
      width: 160,
      height: 120,
      supported: true
    },
    /*{
      name: 'QCIF',
      width: 176,
      height: 144,
      supported: true
    },*/
    {
      name: 'QVGA',
      width: 320,
      height: 240,
      supported: true
    },
    /*{
      name: 'CIF',
      width: 352,
      height: 288,
      supported: true
    },*/
    {
      name: '360p(nHD)',
      width: 640,
      height: 360,
      supported: true
    },
    {
      name: 'VGA',
      width: 640,
      height: 480,
      supported: true
    },
    /*{
      name: 'SVGA',
      width: 800,
      height: 600,
      supported: true
    },*/
    {
      name: 'HD',
      width: 1280,
      height: 720,
      supported: true
    },
    /*{
      name: 'UXGA',
      width: 1600,
      height: 1200,
      supported: true
    },*/
    {
      name: 'Full HD',
      width: 1920,
      height: 1080,
      supported: true
    },
    /*{
      name: '4K',
      width: 4096,
      height: 2160,
      supported: true
    }*/
  ];

  public selectedResolution = {
    width: 640,
    height: 360,
    index: 2
  };
  /*public selectedResolution = {
    width: 1920,
    height: 1080,
    index: 5
  };*/

  public resolutionsFetched = false;

  @HostListener('window:popstate', ['$event'])
  public async onPopState($event) {
    /*const option = alert('Are you sure you want to leave this meeting?');
    console.log(option);
    if (option) {
      await this.stopStreaming();
    } else {
      history.back();
    }*/
  }

  async ngOnInit() {
    const meetingInProgress = await localStorage.getItem('meetingInProgress');
    this.currentMeeting = await JSON.parse(localStorage.getItem('currentMeeting'));
    this.meetingDetails = await JSON.parse(localStorage.getItem('meetingDetails'));
    this.currentAttendee = await JSON.parse(localStorage.getItem('currentAttendee'));
    const localID = await JSON.parse(localStorage.getItem('localID'));
    const waiting = await localStorage.getItem('waiting');
    if (this.currentAttendee !== null && this.currentAttendee.role !== null && this.currentAttendee.role !== 'host') {
      this.meetingType = 'Join Meeting';
    }
    if (meetingInProgress !== null && meetingInProgress === '1') {
      await this.router.navigate(['meeting-in-progress']);
    } else if (this.currentMeeting !== null && this.currentAttendee !== null && waiting === '1') {
      if (localID !== null) {
        await this.owtService.deleteParticipant(this.currentMeeting.owt_meeting_id, localID);
      }
      await this.router.navigate(['/meeting', 'meeting-room']);
    } else if (this.currentMeeting === null || this.currentAttendee === null) {
      await this.router.navigate(['/meeting']);
    } else {
      const participants = await this.meetingService.getMeetingAttendees(this.currentMeeting._id);
      this.participantNames = await this.getParticipantNames(participants);
      this.sources = await this.fetchDevices();
      const audioSource = this.mediaPermissions.audio ? this.sources.audioIn[0].deviceId : null;
      const videoSource = this.mediaPermissions.video ? this.sources.video[0].deviceId : null;
      if (audioSource || videoSource) {
        await this.startStreaming(audioSource, videoSource, this.selectedResolution);
      } else {
        this.cameraVideoLoading = false;
      }
      if (this.mediaPermissions.audio) {
        await this.startListening();
      }
    }

    /*this.socketSubscription = this.meetingService.attendeeSocket.subscribe(async (msg) => {
      if (msg.id === this.currentAttendee._id) {
        await localStorage.clear();
        await this.onLeavePage();
        await this.router.navigate(['/meeting']);
      }
    });*/
  }

  async ngOnDestroy() {
    /*if (this.socketSubscription) {
      await this.socketSubscription.unsubscribe();
    }*/
    await this.onLeavePage();
  }

  async fetchDevices() {
    return new Promise(async (resolve) => {
      await navigator.mediaDevices.getUserMedia({audio: true, video: true}).then(
        async (stream) => {
          await navigator.mediaDevices.enumerateDevices().then(devices => {
            devices.forEach(device => {
              // console.log(device);
              if (device.kind === 'audioinput') {
                this.sources.audioIn.push(device);
              } else if (device.kind === 'audiooutput') {
                this.sources.audioOut.push(device);
              } else if (device.kind === 'videoinput') {
                this.sources.video.push(device);
              }
            });
            if (this.sources.audioIn.length === 0) {
              this.micError = 'Microphone not found!';
            }
            if (this.sources.video.length === 0) {
              this.cameraError = 'Camera not found!';
            }
          });
          this.mediaPermissions.audio = true;
          this.mediaPermissions.video = true;
          await this.stopStreaming(stream);
          resolve(this.sources);
        }
      ).catch(async (err) => {

        await navigator.mediaDevices.getUserMedia({audio: true, video: false}).then(
          async (stream) => {
            await navigator.mediaDevices.enumerateDevices().then(devices => {
              devices.forEach(device => {
                if (device.kind === 'audioinput') {
                  this.sources.audioIn.push(device);
                } else if (device.kind === 'audiooutput') {
                  this.sources.audioOut.push(device);
                }
              });
              if (this.sources.audioIn.length === 0) {
                this.micError = 'Microphone not found!';
              }
            });
            this.mediaPermissions.audio = true;
            await this.stopStreaming(stream);
          }
        ).catch(audioErr => {
          console.log(audioErr);
          if (this.sources.audioIn.length === 0) {
            if (audioErr.name === 'NotAllowedError') {
              this.micError = 'Permission Denied';
            } else if (audioErr.name === 'NotFoundError') {
              this.micError = 'Can not find mic';
            } else {
              this.micError = 'Mic is not accessible';
            }
          }
        });

        await navigator.mediaDevices.getUserMedia({audio: false, video: true}).then(
          async (stream) => {
            await navigator.mediaDevices.enumerateDevices().then(devices => {
              devices.forEach(device => {
                if (device.kind === 'videoinput') {
                  this.sources.video.push(device);
                }
              });
              if (this.sources.video.length === 0) {
                this.cameraError = 'Camera not found!';
              }
            });
            this.mediaPermissions.video = true;
            await this.stopStreaming(stream);
          }
        ).catch(videoErr => {
          console.log(videoErr);
          this.resolutions = [];
          if (this.sources.video.length === 0) {
            if (videoErr.name === 'NotAllowedError') {
              this.cameraError = 'Permission Denied';
            } else if (videoErr.name === 'NotFoundError') {
              this.cameraError = 'Can not find Camera';
            } else {
              this.cameraError = 'Camera is not accessible';
            }
          }
        });

        resolve(this.sources);


      });
    });
  }

  async attachDeviceId(element, deviceId) {
    if (typeof element.sinkId !== 'undefined') {
      await element.setSinkId(deviceId)
        .catch(error => {
          let errorMessage = error;
          if (error.name === 'SecurityError') {
            errorMessage = `You need to use HTTPS for selecting audio output device: ${error}`;
          }
          alert(errorMessage);
          this.audioDestination.nativeElement.value = this.sources.audioOut[0].deviceId;
        });
    } else {
      alert('Browser does not support output device selection.');
    }
  }

  async onDestinationChange() {
    await this.attachDeviceId(this.videoPreview.nativeElement, this.audioDestination.nativeElement.value);
  }

  async startStreaming(audioSource, videoSource, resolution = null) {
    this.cameraVideoLoading = true;
    let constraints;
    if (resolution === null) {
      constraints = {
        audio: audioSource ? {deviceId: {exact: audioSource}, echoCancellation: true} : false,
        video: videoSource ? {deviceId: {exact: videoSource}} : false
      };
    } else {
      constraints = {
        audio: audioSource ? {deviceId: {exact: audioSource}, echoCancellation: true} : false,
        video: videoSource ? {
          deviceId: {exact: videoSource},
          width: {exact: resolution.width},
          height: {exact: resolution.height}
        } : false
      };
    }
    console.log(constraints);

    await this.stopStreaming(this.mediaStream);
    await navigator.mediaDevices.getUserMedia(constraints).then(async (stream) => {
      this.videoPreview.nativeElement.srcObject = stream;
      this.videoPreview.nativeElement.volume = 0;
      this.mediaStream = stream;
      if (this.isSettingsModalOpen) {
        await this.stopStreaming(stream);
        this.videoPreview.nativeElement.srcObject = null;
      }
      this.cameraVideoLoading = false;
    }).catch(async err => {
      if (this.mediaStatus.video !== false && this.mediaPermissions.video !== false) {
        await this.fallBackStreaming(constraints);
      }
      console.log(err, err.name, err.message);
    });
  }

  async fallBackStreaming(constraints) {
    await this.fetchResolutions();
    const supportedResolutions = [];
    this.resolutions.forEach(resolution => {
      if (resolution.supported === true) {
        supportedResolutions.push(resolution);
      }
    });
    console.log(supportedResolutions);
    const newConstraints = constraints;
    if (supportedResolutions.length === 0) {
      newConstraints.video = false;
      alert('Your device doesn\'t support any video resolution!');
      this.videoPreview.nativeElement.srcObject = null;
      this.mediaStatus.video = false;

    } else {
      newConstraints.video.width = {exact: supportedResolutions[supportedResolutions.length - 1].width};
      newConstraints.video.height = {exact: supportedResolutions[supportedResolutions.length - 1].height};
    }
    console.log(newConstraints);
    await navigator.mediaDevices.getUserMedia(newConstraints).then(async (stream) => {
      this.mediaStream = stream;
      this.cameraVideoLoading = false;
      console.log(stream);
      if (supportedResolutions.length !== 0) {
        this.videoPreview.nativeElement.srcObject = stream;
        this.videoPreview.nativeElement.volume = 0;
        this.selectedResolution = {
          width: stream.getVideoTracks()[0].getSettings().width,
          height: stream.getVideoTracks()[0].getSettings().height,
          index: supportedResolutions.length - 1
        };
        console.log(stream.getVideoTracks()[0].getSettings().height);
      }
    }).catch(err => {
      alert('Unsupported devices!');
      this.mediaStatus.video = false;
      this.mediaStatus.audio = false;
      this.videoPreview.nativeElement.srcObject = null;
      console.log(err, err.name, err.message);
    });
  }

  async onSourceChange() {
    await this.startStreaming(this.mediaStatus.audio ? this.audioSource.nativeElement.value : false, this.mediaStatus.video ? this.videoSource.nativeElement.value : false, this.selectedResolution);
  }

  async onResolutionChange(event) {
    const resolution = this.resolutions[event.target.selectedIndex];
    await this.stopStreaming(this.settingsMediaStream);
    this.videoSettingPreview.nativeElement.srcObject = null;
    this.settingsCameraVideoLoading = true;
    const constraints = {
      audio: false,
      video: {
        deviceId: {exact: this.videoSource.nativeElement.value},
        width: {exact: resolution.width},
        height: {exact: resolution.height}
      }
    };
    await navigator.mediaDevices.getUserMedia(constraints).then(async (stream) => {
      this.videoSettingPreview.nativeElement.srcObject = stream;
      this.settingsMediaStream = stream;
      this.settingsCameraVideoLoading = false;
    }).catch(err => {
      console.log(err, err.name, err.message);
    });
  }

  async stopStreaming(stream) {
    if (stream !== undefined) {
      await stream.getTracks().forEach(track => {
        track.stop();
      });
    }
  }

  async toggleAudioStatus() {
    if (this.cameraVideoLoading) {
      this.toasterService.info('Please while we set up the meeting for you.');
      return;
    }
    if (this.mediaStream !== undefined) {
      if (this.mediaStatus.audio) {
        this.mediaStatus.audio = false;
        await this.mediaStream.getTracks().forEach((track) => {
          if (track.kind === 'audio') {
            //todo: try to put in async
            track.stop();
          }
        });
      } else {
        this.mediaStatus.audio = true;
        await this.startStreaming(this.audioSource.nativeElement.value, this.mediaStatus.video ? this.videoSource.nativeElement.value : false, this.selectedResolution);
      }
    }
  }

  async toggleVideoStatus() {
    if (this.cameraVideoLoading) {
      this.toasterService.info('Please while we set up the meeting for you.');
      return;
    }
    if (this.mediaStream !== undefined) {
      if (this.mediaStatus.video) {
        this.mediaStatus.video = false;
        this.videoPreview.nativeElement.srcObject = null;
        await this.mediaStream.getTracks().forEach((track) => {
          if (track.kind === 'video') {
            track.stop();
          }
        });
      } else {
        this.mediaStatus.video = true;
        await this.startStreaming(this.mediaStatus.audio ? this.audioSource.nativeElement.value : false, this.videoSource.nativeElement.value, this.selectedResolution);
      }
    }
  }

  async onSubmit() {
    if (this.cameraVideoLoading) {
      this.toasterService.info('Please while we set up the meeting for you.');
      return;
    }
    const mediaConstraints = {
      audio: {
        status: this.mediaStatus.audio,
        source: this.audioSource.nativeElement.value,
        destination: this.audioDestination.nativeElement.value
      },
      video: {
        status: this.mediaStatus.video,
        source: this.videoSource.nativeElement.value,
        resolution: this.selectedResolution
      }
    };
    await this.stopStreaming(this.mediaStream);
    this.currentAttendee = await this.meetingService.addMeetingAttendee(this.currentMeeting._id,
      {
        name: this.currentAttendee.name !== undefined ? this.currentAttendee.name : '',
        role: this.currentAttendee.role !== undefined ? this.currentAttendee.role : 'attendee'
      })
      .catch((err) => {
        console.log('Meeting Attendee Creation Error: ', err);
      });
    await localStorage.setItem('currentAttendee', JSON.stringify(this.currentAttendee));
    await localStorage.setItem('waiting', '1');
    await localStorage.setItem('mediaConstraints', JSON.stringify(mediaConstraints));
    await localStorage.setItem('devices', JSON.stringify(this.sources));

    // !!!CHANGE LATER
    if (!this.mediaPermissions.audio) {
      this.mediaPermissions.video = false;
    }
    await localStorage.setItem('mediaPermissions', JSON.stringify(this.mediaPermissions));
    await this.router.navigate(['/meeting', 'meeting-room']);
  }

  async getParticipantNames(participants) {
    const count = await this.helperService.validLength(participants);
    let participantNames = 'Active Participants : ';
    if (count === 0) {
      participantNames = 'No Active Participants';
    } else if (count === 1) {
      participantNames += participants[0].name;
    } else if (count === 2) {
      participantNames += participants[0].name + ' and ' + participants[1].name;
    } else if (count === 3) {
      participantNames += participants[0].name + ', ' + participants[1].name + ' and ' + participants[2].name;
    } else if (count === 4) {
      participantNames += participants[0].name + ', ' + participants[1].name + ', ' + participants[2].name + ' and 1 other';
    } else if (count > 4) {
      participantNames += participants[0].name + ', ' + participants[1].name + ', ' + participants[2].name + ' and ' + (count - 3) + ' others';
    }
    return participantNames;
  }

  async onClickSettings() {
    this.isSettingsModalOpen = true;
    if (!this.mediaPermissions.video) {
      return;
    }
    this.settingsCameraVideoLoading = true;
    await this.stopStreaming(this.mediaStream);
    this.videoPreview.nativeElement.srcObject = null;
    if (!this.resolutionsFetched) {
      await this.fetchResolutions();
    }
    if (!this.isSettingsModalOpen) {
      return;
    }

    const constraints = {
      audio: false,
      video: {
        deviceId: {exact: this.videoSource.nativeElement.value},
        width: {exact: this.selectedResolution.width},
        height: {exact: this.selectedResolution.height}
      }
    };

    await this.stopStreaming(this.settingsMediaStream);
    await navigator.mediaDevices.getUserMedia(constraints).then(async (stream) => {
      this.videoSettingPreview.nativeElement.srcObject = stream;
      this.settingsMediaStream = stream;
      this.settingsCameraVideoLoading = false;
      if (!this.isSettingsModalOpen) {
        await this.stopStreaming(stream);
        this.videoSettingPreview.nativeElement.srcObject = null;
      }
    }).catch(err => {
      console.log(err, err.name, err.message);
    });
  }


  async fetchResolutions() {
    const resolutionsFetchedevent = new Event('resolutionsFetched');
    for (const resolution of this.resolutions) {
      const constraints = {
        video: {
          width: {exact: resolution.width},
          height: {exact: resolution.height}
        }
      };
      try {
        await navigator.mediaDevices.getUserMedia(constraints)
          .then(stream => {
            console.log(resolution.width + ' ' + resolution.height);
            console.log(stream);
            if (stream) {
              stream.getTracks().forEach(track => {
                track.stop();
              });
            }
          });
      } catch (e) {
        resolution.supported = false;
      }
    }
    this.resolutionsFetched = true;
    window.dispatchEvent(resolutionsFetchedevent);
  }

  async onModalClick(event) {
    if (event.target.classList.contains('joinmeeting') || event.target.classList.contains('settingModalClose')) {
      this.cameraVideoLoading = true;
      await new Promise((resolve) => {
        if (this.resolutionsFetched) {
          resolve();
        } else {
          window.addEventListener('resolutionsFetched', () => {
            resolve();
          });
        }
      });
      this.isSettingsModalOpen = false;
      this.settingsCameraVideoLoading = false;
      this.elementRef.nativeElement.querySelector('#resolutionDropDown').selectedIndex = this.selectedResolution.index;
      await this.stopStreaming(this.settingsMediaStream);
      await this.startStreaming(this.mediaStatus.audio ? this.audioSource.nativeElement.value : false,
        this.mediaStatus.video ? this.videoSource.nativeElement.value : false,
        this.selectedResolution);
    }
  }

  async startListening() {
    let audioContext = new AudioContext();
    this.microphone = await audioContext.createMediaStreamSource(this.mediaStream);
    this.javascriptNode = await audioContext.createScriptProcessor(1024, 1, 1);
    await this.microphone.connect(this.javascriptNode);
    await this.javascriptNode.connect(audioContext.destination);
    this.javascriptNode.onaudioprocess = async (event) => {
      const input = await event.inputBuffer.getChannelData(0);
      let i;
      let sum = 0.0;
      for (i = 0; i < input.length; ++i) {
        sum += input[i] * input[i];
      }
      this.instant = await Math.sqrt(sum / input.length).toFixed(2);
      await this.ngZone.run(async () => {
        this.instant *= 100;
      });
    };
  }

  async onLeavePage() {
    if (this.mediaStream !== undefined) {
      await this.stopStreaming(this.mediaStream);
    }
    if (this.settingsMediaStream !== undefined) {
      await this.stopStreaming(this.settingsMediaStream);
    }
    if (this.microphone) {
      await this.microphone.disconnect();
    }
    if (this.javascriptNode) {
      await this.javascriptNode.disconnect();
    }
  }

  async applySettings() {
    this.cameraVideoLoading = true;
    await new Promise((resolve) => {
      if (this.resolutionsFetched) {
        resolve();
      } else {
        window.addEventListener('resolutionsFetched', () => {
          resolve();
        });
      }
    });
    this.isSettingsModalOpen = false;
    this.settingsCameraVideoLoading = false;
    await this.stopStreaming(this.settingsMediaStream);
    this.videoSettingPreview.nativeElement.srcObject = null;
    const selectedIndex = this.elementRef.nativeElement.querySelector('#resolutionDropDown').selectedIndex;
    const resolution = this.resolutions[selectedIndex];
    this.selectedResolution.width = resolution.width;
    this.selectedResolution.height = resolution.height;
    this.selectedResolution.index = selectedIndex;
    await this.startStreaming(this.mediaStatus.audio ? this.audioSource.nativeElement.value : false, this.mediaStatus.video ? this.videoSource.nativeElement.value : false, this.selectedResolution);
  }

  onCopy() {
    this.renderer.addClass(this.copyButton.nativeElement, 'copied');
    /*this.toasterService.info('copied', '', {
      disableTimeOut: true
    });*/
    setTimeout(() => {
      this.renderer.removeClass(this.copyButton.nativeElement, 'copied');
    }, 600);
  }
}
