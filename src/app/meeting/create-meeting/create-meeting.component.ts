import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
//import * as Owt from 'scripts/owt.js';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MeetingService} from '../meeting.service';
import {OwtService} from '../owt.service';

//import {consoleTestResultHandler} from 'tslint/lib/test';

@Component({
  selector: 'app-create-meeting',
  templateUrl: './create-meeting.component.html',
  styleUrls: ['./create-meeting.component.css']
})
export class CreateMeetingComponent implements OnInit {

  meetingForm: FormGroup;
  meetingUrl: any;

  constructor(private router: Router, private meetingService: MeetingService, private owtService: OwtService, private fb: FormBuilder) {
    this.createMeetingFormGroup();
  }

  async ngOnInit() {
    const currentMeeting = await JSON.parse(localStorage.getItem('currentMeeting'));
    const attendeeData = await JSON.parse(localStorage.getItem('attendeeData'));
    const currentAttendee = await JSON.parse(localStorage.getItem('currentAttendee'));
    const lastMeeting = await JSON.parse(localStorage.getItem('lastMeeting'));

    if (currentMeeting !== null && attendeeData !== null && currentAttendee === null) {
      await this.router.navigate(['/meeting', 'waiting-room']);
    } else if (currentMeeting !== null && attendeeData !== null && currentAttendee !== null) {
      await this.router.navigate(['/meeting', 'meeting-room']);
    } else if (lastMeeting !== null) {
      await this.router.navigate(['/meeting', 'feedback']);
    }
    //await localStorage.clear();
  }

  createMeetingFormGroup() {
    this.meetingForm = this.fb.group({
      host_name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(250)]]
    });
  }

  async createMeeting() {
    let meeting = this.meetingForm.value;
    meeting.name = Math.random().toString(36).substring(2, 8) + Math.random().toString(36).substring(2, 8)
    meeting.meeting_code = Math.random().toString(36).substring(2, 5) + Math.random().toString(36).substring(2, 5);
    const isValid = this.meetingForm.valid;
    const hostName = meeting.host_name;
    delete meeting.host_name;
    console.log(meeting, isValid);

    if (isValid) {
      try {
        let meetings = await this.meetingService.index({name: meeting.name});
        if (meetings.length > 0) {
          alert('Meeting Room already exist with this name!');
        } else {
          let owtRoom = await this.owtService.createRoom(meeting.name);
          console.log('OWT Room : ' + owtRoom);
          meeting.owt_meeting_id = owtRoom.id;
          console.log('Meeting Data : ' + meeting);
          let meetingRoom = await this.meetingService.createMeeting(meeting);
          if (meetingRoom) {
            console.log('Meeting Room : ' + meetingRoom);
            await localStorage.setItem('currentMeeting', JSON.stringify(meetingRoom));
            await localStorage.setItem('attendeeData', JSON.stringify({name: hostName, role: 'host'}));
            this.meetingUrl = window.location.protocol + '//' + window.location.host + '/#/meeting/join?room_id=';
            this.meetingUrl = encodeURI(this.meetingUrl + meeting.name + '&meeting_code=' + meeting.meeting_code);
            localStorage.setItem('meetingDetails', JSON.stringify({name: meeting.name, meetingCode: meeting.meeting_code, meetingUrl: this.meetingUrl}));
            await this.router.navigate(['/meeting', 'waiting-room']);
          }
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      alert('Please fill all the fields!');
    }
  }

}
