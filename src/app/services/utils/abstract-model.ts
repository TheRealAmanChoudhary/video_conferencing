import {IModel} from './model';

export abstract class AbstractModel implements IModel {
  constructor() {
  }

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
};
