export interface IModel {
  deserialize(input: any): this;
}
