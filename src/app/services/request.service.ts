import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(public http: HttpClient) {
  }


  public async request(method: string, url: string, data?: any) {
    const result = this.http.request(method, url, {
      body: data,
      responseType: 'json',
      observe: 'body',
    });

    return new Promise<any>((resolve, reject) => {
      result.subscribe(resolve as any, reject as any);
    });
  }

  public async fileRequest(method: string, url: string, data?: any) {
    const result = this.http.request(method, url, {
      body: data,
      responseType: 'blob',
      observe: 'body',
    });

    return new Promise<any>((resolve, reject) => {
      result.subscribe(resolve as any, reject as any);
    });
  }

  public observableRequest(method: string, url: string, data?: any): any {
    return Observable.create(observer => {
      this.http.request<any>(method, url, {
        body: data,
        responseType: 'json',
        observe: 'body',
      }).subscribe(res => {
          observer.next(res);
        },
        err => {
          observer.error(err);
        }
      );
    });
  }

  public send(method, path, body, onRes) {
    let req = new XMLHttpRequest();
    req.onreadystatechange = function () {
      if (req.readyState === 4) {
        onRes(req.responseText);
      }
    };

    req.open(method, path, true);
    req.setRequestHeader('Content-Type', 'application/json');
    if (body !== undefined) {
      req.send(JSON.stringify(body));
    } else {
      req.send();
    }
  }

}
