import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelperService {
  length(val) {
    try {
      const len = val.length;
      return len;
    } catch (e) {
      return -1;
    }
  }

  getAvatarText(str) {
    str = str.trim();
    if (str.length === 0) {
      return '';
    }
    if (str.length === 1) {
      return str.toUpperCase();
    }
    const parts = str.split(' ');
    if (parts.length === 1) {
      return (parts[0][0]).toUpperCase();
    }
    return (parts[0][0] + parts[parts.length - 1][0]).toUpperCase();
  }

  async validLength(val) {
    try {
      let count = 0;
      await val.forEach(value => {
        if (!!value) {
          count++;
        }
      });
      return count;
    } catch (e) {
      return -1;
    }
  }

  msToTime(duration) {
    let milliseconds = parseInt(((duration % 1000)).toString(), 10);
    let seconds: string | number = parseInt(((duration / 1000) % 60).toString(), 10);
    let minutes: string | number = parseInt(((duration / (1000 * 60)) % 60).toString(), 10);
    let hours: string | number = parseInt(((duration / (1000 * 60 * 60)) % 24).toString(), 10);
    hours = (hours < 10) ? '0' + hours : hours;
    minutes = (minutes < 10) ? '0' + minutes : minutes;
    seconds = (seconds < 10) ? '0' + seconds : seconds;

    if (hours === '00') {
      return minutes + ':' + seconds;
    }
    return hours + ':' + minutes + ':' + seconds;
  }

  emptyIfNull(val) {
    try {
      if (!!val) {
        return '';
      } else {
        return val;
      }
    } catch (e) {
      return '';
    }
  }

  log(val) {
    console.log(val);
  }
}
