import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class Config {
  public rootUrl = environment.rootUrl;
  public owtUrl = environment.owtUrl;
  public siteKey = environment.siteKey;
  public roomConfig = environment.roomConfig;
}
