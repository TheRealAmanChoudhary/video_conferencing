import {Component, OnDestroy, Inject, ViewEncapsulation} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DefaultLayoutComponent {

  constructor(private router: Router) {
  }

}
