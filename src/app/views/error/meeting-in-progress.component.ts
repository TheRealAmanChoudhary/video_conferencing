import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    templateUrl: 'meeting-in-progress.component.html'
})
export class MeetingInProgressComponent implements OnInit {

    constructor(private router: Router) {
    }

    async ngOnInit() {
        await localStorage.setItem('meetingInProgress', '1');
        /*const meetingInProgress = await localStorage.getItem('meetingInProgress');
        if (meetingInProgress === null || meetingInProgress !== '1') {
            return await this.router.navigate(['meeting']);
        }*/
    }

}
