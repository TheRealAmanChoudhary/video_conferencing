import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TokenStorage} from "../services/token.storage";

export class RequestInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const tokenObject = new TokenStorage();
    const token = tokenObject.getToken();
    const request = req.clone({
      headers: req
        .headers
        .set('Authorization', token ? `Bearer ${token}` : '')
    });

    return next.handle(request);
  }
}
