import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {HelperService} from '../services/helper.service';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
  constructor(private helper: HelperService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError(error => {
        if (error instanceof HttpErrorResponse) {
          console.log(error);
          const text =
            error.error && error.error.message ? error.error.message : error.statusText;
          (<any>window).globalEvents.emit('open error dialog', text);
        }

        return throwError(error);
      })
    );
  }
}
