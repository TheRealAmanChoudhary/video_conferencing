import {BrowserModule} from '@angular/platform-browser';
import {NgModule, Inject} from '@angular/core';
import { init as initApm } from '@elastic/apm-rum';

import {AppComponent} from './app.component';
import {DefaultLayoutComponent} from './containers/default-layout';
import {Router, RouterModule} from '@angular/router';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {RequestInterceptor} from './interceptors/request.interceptor';
import {ResponseInterceptor} from './interceptors/response.interceptor';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {AppRoutingModule} from './app.routing';
import {SharedModule} from './shared/shared.module';
import {WebsocketService} from './meeting/web-socket.service';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RecaptchaModule} from 'angular-google-recaptcha';

import { ApmService } from '@elastic/apm-rum-angular';
import { ErrorHandler } from '@angular/core';
import { ApmErrorHandler } from '@elastic/apm-rum-angular';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

const apm = initApm({

    // Set required service name (allowed characters: a-z, A-Z, 0-9, -, _, and space)
    serviceName: 'vc-acefone-owt-1-rum',

    // Set custom APM Server URL (default: http://localhost:8200)
    serverUrl: 'https://54.39.191.89:8200',

    // Set service version (required for sourcemap feature)
    serviceVersion: ''
});

@NgModule({
  declarations: [
    AppComponent,
    APP_CONTAINERS
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers:
    [
      {
        provide: HTTP_INTERCEPTORS,
        useClass: RequestInterceptor,
        multi: true,
      },
      {
        provide: HTTP_INTERCEPTORS,
        useClass: ResponseInterceptor,
        multi: true,
      },
      {
            provide: ApmService,
            useClass: ApmService,
            deps: [Router]
      },
      {
            provide: ErrorHandler,
            useClass: ApmErrorHandler
      },
      {
        provide: LocationStrategy,
        useClass: HashLocationStrategy
      },
      WebsocketService
    ],
  bootstrap: [AppComponent]
})

export class AppModule {
    constructor(@Inject(ApmService) service: ApmService) {
        // API is exposed through this apm instance
        const apm = service.init({
            serviceName: 'vc-acefone-owt-1-rum',
            serverUrl: 'https://54.39.191.89:8200'
        });

        apm.setUserContext({
            'username': 'foo',
            'id': 'bar'
        });
    }
}
